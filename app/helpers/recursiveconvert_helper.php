<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('to_stdClass')) {
    function to_stdClass($data){
        return json_decode(json_encode($data));
    }
}
if (!function_exists('to_array')) {
    function to_Array($data){
        return json_decode(json_encode($data), TRUE);
    }
}