<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('assets_config')) {
    function assets_config()
    {
        $CI = &get_instance();
        $CI->load->config('assets');

        $config['assets_path'] = $CI->config->item('assets_path');
        $config['node_path'] = $CI->config->item('node_path');
        return $config;
    }
}

if (!function_exists('assets_img')) {
    function assets_img($file, $attr = array())
    {
        $config = assets_config();
        $attribute = '';
        if (!empty($attr) && is_array($attr)) {
            foreach ($attr as $key => $value) {
                $attribute .= ' ' . $key . '="' . $value . '"';
            }
        } elseif (!empty($attr)) {
            $attribute .= $attr;
        }
        return '<img src="' . $config['assets_path'] . $file . '"' . $attribute . '>';
    }
}

if (!function_exists('assets_css') && !function_exists('single_css')) {
    function assets_css($file)
    {
        $config = assets_config();
        $data = "";
        foreach ($file as $result) {
            $data .= "<link rel='stylesheet' href='" . $config['assets_path'] . $result . "'>\r\n";
        }

        return $data;
    }

    function single_css($file)
    {
        $config = assets_config();
        return "<link rel='stylesheet' href='" . $config['assets_path'] . $file . "'>\r\n";
    }
}

if (!function_exists('assets_js') && !function_exists('single_js')) {
    function assets_js($file)
    {
        $config = assets_config();
        $data = "";
        foreach ($file as $result) {
            $data .= "<script type='text/javascript' src='" . $config['assets_path'] . $result . "'></script>\r\n";
        }

        return $data;
    }


    function single_js($file)
    {
        $config = assets_config();
        return "<script type='text/javascript' src='" . $config['assets_path'] . $file . "'></script>\r\n";
    }
}

function single_img($file, $attr = array())
{
    $config = assets_config();
    $attribute = '';
    if (!empty($attr) && is_array($attr)) {
        foreach ($attr as $key => $value) {
            $attribute .= " " . $key . "='" . $value . "'";
        }
    }
    //return "<img src='".$config['assets_path'].$file."'".$attribute.">\r\n";
    return "<img src='" . $config['assets_path'] . $file . "'" . $attribute . ">";
}


if (!function_exists('assets_node') && !function_exists('single_node')) {
    function assets_node($file)
    {
        $config = assets_config();
        $data = "";
        foreach ($file as $result) {
            $data .= "<script src='" . $config['node_path'] . $result . "'></script>\r\n";
        }

        return $data;
    }


    function single_node($file)
    {
        $config = assets_config();
        return "<script src='" . $config['node_path'] . $file . "'></script>\r\n";
    }
}
