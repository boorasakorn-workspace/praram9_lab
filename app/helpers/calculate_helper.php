<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('duration')) {
    function duration($Start, $End = NULL)
    {
        if($End == NULL) $End = date('Y-m-d H:i:s');

        $Remain = intval(strtotime($End) - strtotime($Start));
        $Minutes = floor($Remain/60);
        
        return $Minutes;        
    }
}
