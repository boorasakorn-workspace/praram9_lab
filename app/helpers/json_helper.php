<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('json_response')) {
    function json_response($code = 200, $message = null){
        header_remove();
        http_response_code($code);
        header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
        header('Content-Type: application/json');
        $status = array(
            200 => '200 OK',
            204 => '204 No Content',
            400 => '400 Bad Request',
            404 => '404 Not Found',
            422 => 'Unprocessable Entity',
            500 => '500 Internal Server Error'
            );
        header('Status: '.$status[$code]);
        $response = array(
            'status' => ( $code < 300 ),
            'message' => ( isset($message) && $message ? $message : ( isset($status[$code]) && $status[$code] ? $status[$code] : '' ) ) ,
        );
        return json_encode($response);
    }
}