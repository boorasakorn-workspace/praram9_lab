<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Login/login_main';

$route['lab'] = 'Lab';

$route['api/lab/v1/mobile/(.*)'] = 'api_lab/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
