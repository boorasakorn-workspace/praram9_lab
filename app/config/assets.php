<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['assets_path'] = base_url('static/');
$config['node_path'] = base_url('static/socket.io-client/');