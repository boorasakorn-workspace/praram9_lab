<script type="text/javascript">

    function reverse(str) {
        return str.split('').reverse().join('');
    }
    String.prototype.reverseInsert = function (index, string) {
        let thisString = this;
        thisString = reverse(thisString);
        if (index > 1)
            return reverse(thisString.substring(0, index) + string + thisString.substring(index, thisString.length));
        
        return string + thisString;        
    };

    //Const Function
        //initDataTable
            const initDataTable = (SelectTable) => {
                let DataTable = $(SelectTable).DataTable({
                    ordering: false,
                    scrollY: true,
                    paging: false,
                    dom: 'rt',
                });
                return DataTable;
            }
        //UpdateNumber
            const UpdateNumber = (SelectTable) => {
                console.log("UpdateNumber");
                $(SelectTable + " tr").each(function(index){
                    $(this).find('td:eq(0)').text(index);
                });
            }
        //UpdateWaiting
            const UpdateWaiting = () => {
                console.log("Update Waiting Queue");
                let UpdateURL = "<?=base_url('Lab/getQueue_Waiting/');?>";
                $.getJSON(UpdateURL, { "_": $.now() })
                    .done(function(update_response){
                        let WaitingQueue = update_response['message']['data'];
                        for (let i in WaitingQueue) {  
                            console.log(WaitingQueue[i]['queueno'] + ' : ' + WaitingQueue[i]['queue_waiting_time']);
                            $('[td_waitingqueue="'+WaitingQueue[i]['queueno']+'"]').html(WaitingQueue[i]['queue_waiting_time']);
                        }
                    });
            }
        //Sorting
            // Column 2 = Queueno , Column 5 = WaitingTime
            const TableSort = (SelectTable,Column = 5) => {
                var sorted = $(SelectTable + ' tbody tr').sort(function(a, b) {
                    let eq = ':eq(' + Column + ')';
                    var a = $(a).find('td'+eq).text(), b = $(b).find('td'+eq).text();
                    return b.localeCompare(a, false, {numeric: true})
                })

                $(SelectTable + ' tbody').html(sorted);
            }
        //GetTables
            const GetTable = () => {
                console.log("Get Table");
                swal({
                    title: "Loading...",
                    text: "กรุณารอสักครู่",
                    imageUrl: "<?=base_url('static/img/loading/loading_1.gif');?>",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
                let GetURL = "<?=base_url('Lab/getTable/');?>";
                $.getJSON(GetURL, { "_": $.now() })
                    .done(async function(get_response){
                        let TableStatus = get_response['message']['data'];
                        for (let i in TableStatus) {
                            if(TableStatus[i]['active'] != 'Y' && TableStatus[i]['active'] != true){
                                $('#lab_call #list_call button[data-tableuid="'+TableStatus[i]['uid']+'"]').prop( "disabled", true );
                                if( !$('#list_room button[data-tableuid="'+TableStatus[i]['uid']+'"]').hasClass('disable') ){
                                    await $('#list_room button[data-tableuid="'+TableStatus[i]['uid']+'"]').addClass('disable');
                                    //await $('#lab_call button[data-tableuid="'+TableStatus[i]['uid']+'"]').addClass('disable');
                                }
                            }else{
                                //$('#lab_call #list_call button[data-tableuid="'+TableStatus[i]['uid']+'"]').prop( "disabled", false );
                                if( $('#list_room button[data-tableuid="'+TableStatus[i]['uid']+'"]').hasClass('disable') ){
                                    await $('#list_room button[data-tableuid="'+TableStatus[i]['uid']+'"]').removeClass('disable');
                                    await $('#lab_call button[data-tableuid="'+TableStatus[i]['uid']+'"]').removeClass('disable');
                                }
                            }
                        }
                        await swal.close();
                    })
                    .fail(function(){
                        swal.close();
                    });
            }
        //RenderView
            const RenderView = () => {
                $('.page_row a button').each(function(e){
                    if($(this).hasClass('active')){
                        thisButton = $(this);
                        buttonParentRow = $(this).closest('.page_row');

                        let getPage = $(this).closest('a').data('subtable');
                        $.get(getPage, { "_": $.now() })
                            .done(function(response) {
                                let target = response['message']['target'];
                                let res_html = response['message']['html'];
                                $(target).html(res_html);
                                $(buttonParentRow).find('button').removeClass('active');
                                $(thisButton).addClass('active');
                                TableSort('#dataTable');
                                UpdateWaiting();
                                UpdateNumber('#dataTable');
                            });
                    }
                })
            }
        //UpdateWaitingQueue
            const updateWaitingQueue = () => {
                 setInterval(function(){
                        $('[td_waitingqueue]').each(function(index){
                            let Minute = parseInt( $(this).html() );                            
                            if( parseInt($(this).html()) < 15 && parseInt(Minute+1) >= 15){
                                $(this).css('color','#FF0000');
                            }
                            $(this).html( Minute+1 );
                        });
		    		}, 60000);
            }
</script>

<script type="text/javascript">
    updateWaitingQueue();
    var user_uid = "<?=json_encode($this->session->userdata('userlogin')['uid']);?>";
    var user_table;
    console.log('<?=json_encode($this->session->userdata('userlogin'));?>');
    var user_floor = '<?=$this->session->userdata('userlogin')['locationuid'];?>';    
    if('<?=$this->session->userdata('userlogin')['locationuser'];?>' == ''){
        $('#desk_span').html(user_floor != 0 && user_floor != '' ? "โต๊ะเจาะเลือดชั้นที่ " + user_floor : 'ไม่พบเซสชั่นกรุณาล็อกอินใหม่อีกครั้ง');
        /*
            switch(<?=json_encode($this->session->userdata('userlogin')['username']);?>) {
                case 'lab01':
                    user_table = 1;
                    break;
                case 'lab02':
                    user_table = 2;
                    break;
                case 'lab03':
                    user_table = 3;
                    break;    
                default:
                    user_table = 0;
                    break;
            }
            $('#desk_span').html(user_table != 0 ? "ชั้นที่ " + user_table : 'ไม่พบเซสชั่นกรุณาล็อกอินใหม่อีกครั้ง');
        */
    }else{
        lab_location = '<?=$this->session->userdata('userlogin')['locationuser'];?>';
        $('#desk_span').html(`ห้องเจาะเลือด (${lab_location})`);
    }
    var rememberData = new Array();
    $(document).ready(function() {
        //Bind ChangePage Event (.page_row a)
            var ManagementTable = $('#headDashboard').on('click', '.page_row a', function(e) {
                e.preventDefault();
                thisButton = $(this).find('button');
                buttonParentRow = $(this).closest('.page_row');

                let getPage = $(this).data('page');
                $.get(getPage, { "_": $.now() })
                    .done(function(response) {
                        $('#contentDashboard').html(response['html']);
                        $(buttonParentRow).find('button').removeClass('active');
                        $(thisButton).addClass('active');
                        ManagementTable = initDataTable('#dataTable');
                        TableSort('#dataTable');
                        UpdateWaiting();
                        UpdateNumber('#dataTable');
                        return ManagementTable;
                    });
            });
        //Detect Change QueueTable (#dataTable)
            var rememberTable = $(document).on('DOMSubtreeModified', '#dataTable', function(e){
                let this_queuetable = $(this).data('queuetable');
                let oldTable = rememberTable;
                if( oldTable == undefined || oldTable != this_queuetable ){
                    newTable = this_queuetable;
                    return newTable;
                }
                console.log(rememberTable);
            });
        //Init First Queue Page After Bind ChangePage Event
            var init = "<?=(isset($Data['queuepage']) && $Data['queuepage'] != '' ? $Data['queuepage'] : '');?>";
            if( init != "" ){ 
                $('.page_row a[data-page="'+init+'"] button').click();
            }
        
        $(document).on('click','[button_floor]', function(e){
            window.stop();
            window.open('<?=base_url('lab/viewcountpatient');?>', "popupWindow", "width=450,height=600,scrollbars=yes");
        });

        $(document).on('search', '#ScanCheckIn', function(){
            var ScanHN = ($(this).val()).trim();
            ScanHN = ScanHN.replace(/^-+/i, '');
            console.log(ScanHN);
            var PostURL = "<?=base_url('Lab/ScanPatient');?>";
            var PostData = {
                    'hn': ScanHN,
                    'patienttype': 0,
                };
            $.post(PostURL, PostData)
                .done(function(response){
                    console.log(response);
                    if(response['message']['result'] == 'Success' || response['message']['result'] == true ){
                        if( $('#Lab_Queue_TBody tr .dataTables_empty').length ){
                            $('#Lab_Queue_TBody').html(response['message']['html']);
                        }else{
                            $('#Lab_Queue_TBody').append(response['message']['html']);
                        }
                        TableSort('#dataTable');
                        UpdateNumber('#dataTable');
                        $("#ScanCheckIn").val('');
                    }else if(response['message']['method'] == 'Insert' && ( response['message']['result'] == 'Failed' || response['message']['result'] == false)){
                        swal({
                            title: "คิวของ HN " + ScanHN,
                            text: "คิวอยู่ในระบบแล้ว",
                            type: "error",
                            confirmButtonClass: "btn-danger",  
                            confirmButtonText: "Close",
                            timer: 2000
                        });
                        $("#ScanCheckIn").val('');
                    }else if(response['message']['method'] == 'Scan' && ( response['message']['result'] == 'Failed' || response['message']['result'] == false)){
                        swal({
                            title: "HN " + ScanHN,
                            text: "ไม่พบข้อมูล",
                            type: "error",
                            confirmButtonClass: "btn-danger",  
                            confirmButtonText: "Close",
                            timer: 2000
                        });
                        $("#ScanCheckIn").val('');
                    }
                })
                .fail(function(){
                    swal({
                        title: "Error",
                        type: "error",
                        confirmButtonClass: "btn-danger",  
                        confirmButtonText: "Close",
                        timer: 1000
                    });
                    $("#ScanCheckIn").val('');
                });
        });

        $(document).on('search', '#ScanCheckIn_Location', function(){
            var ScanHN = ($(this).val()).trim();
            ScanHN = ScanHN.replace(/^-+/i, '');
            console.log(ScanHN);
            var PostURL = "<?=base_url('api_lab/insert');?>";
            var PostData = {
                    'hn': ScanHN,
                    'location':$(this).data('location'),
                };
            $.post(PostURL, PostData).always(function(){
                RenderView();
            });
        });

        // NOTE
            $(document).on('click', '.action_note', async function() {
                var Data = {
                    'patientuid': $(this).data('patientuid'),
                    'queueno': $(this).data('queueno'),
                };
                console.log("Note" , Data);
                await $.get("<?=base_url('Lab/getQueue_Note/');?>"+Data['patientuid'], { "_": $.now() })
                    .done(function(response){
                        $('#md_note textarea[name="textarea_note"]').val(response['message']['data']['notedetail']);
                    });
                await Object.keys(Data).forEach(function (item) {
                    $('#md_note input[name="'+item+'"]').val(Data[item]);
                });
                await $('#md_note').modal('show');
            });
            
            $(document).on('hidden.bs.modal','#md_note',function(){
                $(this).find('form').find('input').val('');
                $(this).find('form').find('textarea').val('');
            });

            $(document).on('click','#md_note form button[type="submit"]',async function(e){
                e.preventDefault();
                var formAction = $(this).closest('form').attr('action');
                var formData = $(this).closest('form').serialize();
                await $.post(formAction, formData)
                    .done(function(response){
                        console.log(response);
                        var active_target = response['message']['active'];
                        if( !$(active_target).hasClass('active') ) $(active_target).addClass('active');                
                    });
                await $(this).closest('.modal').modal('hide');
            });
        // NOTE

        // HOLD
        $(document).on('click', '.action_hold', async function() {
                var Data = {
                    'patientuid': $(this).data('patientuid'),
                    'queueno': $(this).data('queueno'),
                };
                console.log("Hold" , Data);
                await $.get("<?=base_url('Lab/getQueue_Hold/');?>"+Data['patientuid'], { "_": $.now() })
                    .done(function(response){
                        console.log(response);
                        if( response['message']['result'] == 'Failed' || response['message']['data']['messageuid'] == null){
                            $('#md_hold select[name="messageuid"]').val("null");                                                    
                        }else if( response['message']['data']['messageuid'] == 0){
                            $('#md_hold select[name="messageuid"]').val(response['message']['data']['messageuid']);
                            $('input[name="remake"]').prop('disabled',false);
                            $('#md_hold input[name="remake"]').val(response['message']['data']['message']);
                        }else{
                            $('#md_hold select[name="messageuid"]').val(response['message']['data']['messageuid']);
                            $('input[name="remake"]').prop('disabled',true);
                        }                        
                    });
                await $('input[name="remake"]').prop('disabled', $('#md_hold form select[name="messageuid"]').val() == 0?false:true );                
                await Object.keys(Data).forEach(function (item) {
                    $('#md_hold input[name="'+item+'"]').val(Data[item]);
                });
                await $('#md_hold').modal('show');
            });
            
            $(document).on('hidden.bs.modal','#md_hold',function(){
                $(this).find('form').find('select').val('');
                $(this).find('form').find('input').val('');
            });

            $(document).on('change','#md_hold form select[name="messageuid"]', function(e){
                if( $(this).val() == 0){
                    $('input[name="remake"]').prop('disabled',false);
                }else{
                    $('input[name="remake"]').val('');
                    $('input[name="remake"]').prop('disabled',true);
                }
            })

            $(document).on('click','#md_hold form button[type="submit"]',async function(e){
                e.preventDefault();
                if( $('#md_hold select[name="messageuid"]').val() != "null" ){
                    var formAction = $(this).closest('form').attr('action');
                    var formData = $(this).closest('form').serialize();
                    await $.post(formAction, formData)
                        .done(function(response){
                            console.log(response);
                            var active_target = response['message']['active'];
                            if( !$(active_target).hasClass('active') ) $(active_target).addClass('active');                
                        });
                    await $(this).closest('.modal').modal('hide');
                }else{
                    alert("ระบุเหตุผล");
                }
            });
        // HOLD

        // CALL
            $(document).on('click', '.action_call', async function() {
                //Call Modal
                var Data = {
                    'patientuid': $(this).data('patientuid'),
                    'queueno': $(this).data('queueno'),
                    'nation': $(this).data('nation'),
                };
                await Object.keys(Data).forEach(function (item) {
                    $('#lab_call #list_call button').data(item,Data[item]);   
                    $('#lab_call button.action_close').data(item,Data[item]);           
                    $('#lab_call #list_call button').attr('data-'+item,Data[item]);
                    $('#lab_call button.action_close').attr('data-'+item,Data[item]);
                });
                // await GetTable();
                await $('#lab_call').modal('show');
            });

            $(document).on('click','#list_call [data-tableuid]',function(){
                var PostURL = "<?=base_url('Lab/Queue_Call');?>";
                var PostData = {
                    'tableuid': $(this).data('tableuid'),
                    'patientuid': $(this).data('patientuid'),
                    'queueno': $(this).data('queueno'),
                    'nation': $(this).data('nation'),
                };
                $.post(PostURL, PostData)
                    .done(function(response){
                        console.log(response);
                        var active_target = response['message']['active'];
                        if( !$(active_target).hasClass('active') ) $(active_target).addClass('active');
                    });
                console.log(PostData);
            });

            $(document).on('hidden.bs.modal','#lab_call',function(){
                var Data = ['patientuid','queueno'];
                Data.forEach(item => {
                    $(this).find('#list_call').find('button').data(item,'');
                    $(this).find('.action_close').data(item,'');
                    $(this).find('#list_call').find('button').attr('data-'+item,'');
                    $(this).find('.action_close').attr('data-'+item,'');
                });
            });
        // CALL

        // CLOSE
            $(document).on('click', '.action_close', function() {
                //Close
                var PostURL = "<?=base_url('Lab/Queue_Close');?>";
                var PostData = {
                    'patientuid': $(this).data('patientuid'),
                    'queueno': $(this).data('queueno'),
                };
                $.post(PostURL, PostData)
                    .done(async function(response){
                        console.log(response);
                        var remove_target = response['message']['remove'];
                        if($(remove_target).length) await $(remove_target).remove();
                        await UpdateWaiting();
                        if($('#lab_call').hasClass('show')) await $('#lab_call').modal('hide');
                        await TableSort('#dataTable');
                        await UpdateNumber('#dataTable');
                    });
                console.log(PostData);
            });
        // CLOSE

        // CLOSE
            $(document).on('click', '.action_revert', function() {
                //Close
                var PostURL = "<?=base_url('Lab/Queue_Revert');?>";
                var PostData = {
                    'patientuid': $(this).data('patientuid'),
                    'queueno': $(this).data('queueno'),
                };
                $.post(PostURL, PostData)
                    .done(function(response){
                        console.log(response);
                        var remove_target = response['message']['remove'];
                        if($(remove_target).length){
                            $(remove_target).remove();
                        }else{
                            alert("คิวมีอยู่ในรายชื่อแล้ว/ไม่พบคิว ไม่สามารถRevertได้");
                        }
                    });            
                console.log(PostData);
            });
        // CLOSE
        // ROOM (TABLE)
            $(document).on('click', '.lab_on', async function() {
                //Table Modal
                await GetTable();
                await $('#lab_on').modal('show');
            });

            $(document).on('click','[savetoggle]',function(){
                var PostURL = "<?=base_url('Lab/savetoggleTable');?>";
            });
            
            $(document).on('click','#list_room [data-tableuid]',async function(){        
                var PostURL = "<?=base_url('Lab/toggleTable');?>";
                var PostData = {
                    'tableuid': $(this).data('tableuid'),
                    'active': ( $(this).hasClass('disable') ? 'Y' : 'N' ),
                };
                await swal({
                    title: "Loading...",
                    text: "กรุณารอสักครู่",
                    imageUrl: "<?=base_url('static/img/loading/loading_1.gif');?>",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
                await $.post(PostURL, PostData)
                    .done(function(response){
                        console.log(response);
                        GetTable();
                    })
                    .fail(function(err){
                        GetTable();
                    })
            });
        // ROOM

        //Socketio
        const socket = io.connect('<?= APITRIGGER ?>/sc_lab');
        socket.on('patientlab', function(data) {
            console.log("Socket Response : " + "patientlab");
            console.log(data);
            RenderView();
            UpdateWaiting();
            TableSort('#dataTable');
            UpdateNumber('#dataTable');
        });
    });
</script>