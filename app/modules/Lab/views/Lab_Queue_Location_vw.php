<div class="main_container" style="height: 10%;">
    <div class="row no-gutters" style="padding: 0.5rem 2rem 0.5rem .8rem;display:flex;justify-content:space-between;">

        <div class="col-lg-3 col-md-5 col-sm-3">
            <div class="form-inline center" style="padding: .25rem 0rem;">
                <input id="ScanCheckIn_Location" data-location="<?=$this->session->userdata('userlogin')['locationuser'];?>" class="form-control form-control-sm ml-3 w-100 input_icon" type="search" placeholder="Scan Check In" aria-label="Search">
                <!-- <i style="font-size:2rem;margin-left:-4rem;z-index:2;" aria-hidden="true" data-clear="#ScanCheckIn">&times;</i> -->
            </div>
        </div>
        <!-- 
            <div class="row col justify-content-end center_row">
                <button class="lab_on">เปิด/ปิด โต๊ะ</button>
            </div> 
        -->

    </div>
</div>

<!-- Dashboard Table -->
<div class="nowrap_container">
    <table id="dataTable" data-queuetable="queue" class="table table-striped table-bordered  ui-datatable borderless tb-queue" style="max-height: 100%;width:100%; margin-top: 0 !important;">
        <thead>
            <tr>
                <th>#</th>
                <th>Call</th>
                <th>No</th>
                <th>ชื่อ-สกุล</th>
                <th>HN</th>
                <!--
                    <th>VN</th>
                    <th>Slot</th>
                    <th>Type</th>
                    <th>Gender</th>
                    <th>DOB</th>
                -->
                <th>รอ</th>
                <!-- <th>Hold</th> -->
                <th>Note</th>
                <th>เสร็จสิน</th>
            </tr>
        </thead>
        <tbody id="Lab_Queue_TBody">
            <?php 
                if( isset($Data['queuedata']) && count($Data['queuedata']) > 0 ){
                        foreach($Data['queuedata'] as $queue_key => $queue_value):
                            if($queue_value->location && $queue_value->location == $this->session->userdata('userlogin')['locationuser']):
            ?>
                <tr id="RowQueue_<?=$queue_value->queueno;?>">
                    <td><?=$queue_key+1;?></td>
                    <td>
                        <button class="button block btn_action action_call <?=($queue_value->callqueuecwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>" data-nation="<?=$queue_value->nation;?>"><i class="fas fa-volume-up"></i></button>
                    </td>
                    <td><?=$queue_value->queueno;?></td>
                    <td>       
                        <?=$queue_value->prename . ' ' . $queue_value->forename . ' ' . $queue_value->surname;?>
                    </td>
                    <td><?=$queue_value->hn;?></td>
                    <td td_waitingqueue="<?=$queue_value->queueno;?>" <?=$queue_value->queue_waiting_time>14?'style="color: #FF0000"':'';?>><!--<?=duration($queue_value->mwhen);?>--><?=$queue_value->queue_waiting_time;?></td>
                    <!-- <td>
                        <button class="button block btn_action action_hold <?=($queue_value->messagedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fas fa-ban"></i></button>
                    </td> -->
                    <td>
                        <button class="button block btn_action action_note <?=($queue_value->notedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fa fa-edit"></i></button>
                    </td>
                    <td>
                        <button class="button block-nowidth btn_action action_close <?=($queue_value->closed_queue != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><span aria-hidden="true">×</span></button>
                    </td>
                    </tr>
                <?php 
                            endif;
                        endforeach;
                    }
                ?>
        </tbody>
    </table>
</div>
<!-- /Dashboard Table -->

<!-- modal call -->
<div class="modal" tabindex="-1" role="dialog" id="lab_call">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                <h5 class="modal-title">Call</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row col-12 align-items-end  modal-body btn_row">
                <div class="row  col-12 center" id="list_call">
                    <?php foreach($Data['table'] as $T_Key => $T_Val): ?>
                        <div class="col-4">
                            <button class="btnin_call" data-tableuid="<?=$T_Val->uid;?>" data-patientuid="" data-queueno="" ><?=$T_Val->name;?></button>
                            <!-- <?=($T_Val->statusflag != 'Y' && $T_Val->statusflag == TRUE ? 'disabled':'');?> -->
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="row col-12" style="display: none;">
                    <input class="col-4" type="hidden" id="order_location_uid">
                    <input class="col-4" type="hidden" id="patientdetail_uid_counter">
                    <input class="col-4" type="hidden" id="patientxray_uid_counter">
                    <input class="col-4" type="hidden" id="status_type_counter">
                </div>
            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col-3">
                            <button style="color:#000000;" class="button block btn_action action_close active" data-patientuid="" data-queueno="">เสร็จสิ้น</button>
                        </div>
                        <div class="col-3">
                            <button style="color:#000000;" class="button block btn-large" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal hold -->
<div class="modal" tabindex="-1" role="dialog" id="md_hold">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
        <div class="modal-content">
            <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                <h5 class="modal-title">Hold</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body center">
                <div class="container-fluid">
                    <form action="<?= base_url('Lab/Queue_Hold'); ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="queueno">
                        <input type="hidden" name="patientuid">
                        <select id="select_messageuid" name="messageuid" class="form-control">
                            <option value="null">เลือกเหตุผล</option>
                            <?php foreach($Data['holdmessage'] as $message_key => $message_value) : ?>
                                <option value="<?=$message_value->uid;?>"><?=$message_value->description;?></option>
                            <?php endforeach; ?>
                            <option value="0">พิมพ์ข้อความ</option>
                        </select>
                        <div class="row">
                            <div class="col" style="padding: .8rem;">
                                <input class="form-control" id="input_remake" name="remake" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 center">
                                <button class="btn btn-flat btn-success btn-large" type="submit" id='conf_hold'>บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal lab_on -->
<div class="modal" tabindex="-1" role="dialog" id="lab_on">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
                <h5 class="modal-title">เปิด/ปิดโต๊ะ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row col-12 align-items-end  modal-body btn_row center">
                <div class="row  col-12" id="list_room">
                    <?php foreach($Data['table'] as $T_Key => $T_Val): ?>
                        <div class="col-4">
                            <button class="btnin_call <?=($T_Val->statusflag != 'Y' && $T_Val->statusflag == TRUE ? 'disable':'');?>" data-tableuid="<?=$T_Val->uid;?>"><?=$T_Val->name;?></button>
                        </div>   
                    <?php endforeach; ?>
                </div>
                <div class="row col-12" style="display: none;">
                    <input class="col-4" type="hidden" id="order_location_uid">
                    <input class="col-4" type="hidden" id="patientdetail_uid_counter">
                    <input class="col-4" type="hidden" id="patientxray_uid_counter">
                    <input class="col-4" type="hidden" id="status_type_counter">
                </div>
            </div>
            <!--
                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-3">
                                <button style="color:#000000;" class="button block btn-large btn-cyan" savetoggle>บันทึก</button>
                            </div>
                            <div class="col-3">
                                <button style="color:#000000;" class="button block btn-large" data-dismiss="modal">ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            -->
        </div>
    </div>
</div>