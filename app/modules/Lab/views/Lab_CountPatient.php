<table>
    <thead>
        <tr>
            <th>ชั้นที่</th>
            <th>คิวปัจจุบัน</th>
            <th>คิวที่เสร็จสิ้น</th>
            <th>คิวทั้งหมด</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($count as $key => $val) : ?>
            <tr>
                <td><?= $val->locationuid; ?></td>
                <td><?= $val->current; ?></td>
                <td><?= $val->closed; ?></td>
                <td><?= $val->total; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>