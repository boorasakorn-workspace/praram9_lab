<?php 
    if( isset($Data['queuedata']) && count($Data['queuedata']) > 0 ){
        foreach($Data['queuedata'] as $queue_key => $queue_value):
            if($queue_value->location && $queue_value->location == $this->session->userdata('userlogin')['locationuser']):
?>
    <tr id="RowClosedQueue_<?=$queue_value->queueno;?>">
        <td><?=$queue_key+1;?></td>
        <td><?=$queue_value->queueno;?></td>
        <td><?=$queue_value->prename . ' ' . $queue_value->forename . ' ' . $queue_value->surname;?></td>
        <td><?=$queue_value->hn;?></td>
        <td>                        
            <button class="button block btn_action action_note <?=($queue_value->notedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fa fa-edit"></i></button>
        </td>
    </tr>
<?php 
            endif;
        endforeach;
    }
?>