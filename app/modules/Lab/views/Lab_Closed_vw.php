<!-- Dashboard Table -->
<div class="nowrap_container" style="height:100%;">
    <table id="dataTable" data-queuetable="closed" class="table table-striped table-bordered  ui-datatable borderless tb-closed" style="max-height: 100%;width:100%; margin-top: 0 !important;">
        <thead>
            <tr>
                <th>#</th>
                <th>VN</th>
                <th>Name</th>
                <th>HN</th>
                <!-- <th>VN</th> -->
                <!--
                    <th>Slot</th>
                    <th>Type</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>เวลาตรวจเสร็จ</th>
                -->
                <!-- <th>Note</th> -->
                <th>Revert</th>
            </tr>
        </thead>
        <tbody id="Lab_Closed_TBody">
            <!--
                <tr>
                    <td>1</td>
                    <td>No</td>
                    <td>Name</td>
                    <td>HN</td>
                    <td>VN</td>
                    <td>Slot</td>
                    <td>Type</td>
                    <td>Gender</td>
                    <td>DOB</td>
                    <td>เวลาตรวจเสร็จ</td>
                    <td>
                        <button class="button btn_action ">Revert</button>
                    </td>
                    <td>
                        <button class="button btn_action action_note">Note</button>
                    </td>
                </tr>
            -->
            <?php 
                if( isset($Data['queuedata']) && count($Data['queuedata']) > 0 ){
                    foreach($Data['queuedata'] as $queue_key => $queue_value):
                        if(!isset($queue_value->location) || (isset($queue_value->location) && $queue_value->location == NULL)):
            ?>
                <tr id="RowClosedQueue_<?=$queue_value->queueno;?>">
                    <td><?=$queue_key+1;?></td>
                    <td><?=$queue_value->queueno;?></td>
                    <td><?=$queue_value->prename . ' ' . $queue_value->forename . ' ' . $queue_value->surname;?></td>
                    <td><?=$queue_value->hn;?></td>
                    <!-- <td><?=$queue_value->en;?></td> -->
                    <?php /*
                        <td> </td>
                        <td><?=preg_replace('!\d+!', '', $queue_value->queueno);?></td>
                        <td><?=$queue_value->sex;?></td>
                        <td> </td>
                        <td><?=$queue_value->xraydischargedate;?></td>
                    */ ?>
                    <!-- <td>                        
                        <button class="button block btn_action action_note <?=($queue_value->notedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fa fa-edit"></i></button>
                    </td> -->
                    <td>
                        <button class="button block btn_action action_revert" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>">Revert</button>
                    </td>
                </tr>
            <?php 
                        endif;
                    endforeach;
                }
            ?>
        </tbody>
    </table>
</div>
<!-- /Dashboard Table -->