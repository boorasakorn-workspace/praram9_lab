<!-- Dashboard Table -->
<div class="nowrap_container" style="height:100%;">
    <table id="dataTable" data-queuetable="closed" class="table table-striped table-bordered  ui-datatable borderless tb-closed" style="max-height: 100%;width:100%; margin-top: 0 !important;">
        <thead>
            <tr>
                <th>#</th>
                <th>No</th>
                <th>Name</th>
                <th>HN</th>
                <th>Note</th>
            </tr>
        </thead>
        <tbody id="Lab_Closed_TBody">
            <?php 
                if( isset($Data['queuedata']) && count($Data['queuedata']) > 0 ){
                    foreach($Data['queuedata'] as $queue_key => $queue_value):
                        if($queue_value->location && $queue_value->location == $this->session->userdata('userlogin')['locationuser']):
            ?>
                <tr id="RowClosedQueue_<?=$queue_value->queueno;?>">
                    <td><?=$queue_key+1;?></td>
                    <td><?=$queue_value->queueno;?></td>
                    <td><?=$queue_value->prename . ' ' . $queue_value->forename . ' ' . $queue_value->surname;?></td>
                    <td><?=$queue_value->hn;?></td>
                    <td>                        
                        <button class="button block-nowidth btn_action action_note <?=($queue_value->notedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fa fa-edit"></i></button>
                    </td>
                </tr>
            <?php 
                        endif;
                    endforeach;
                }
            ?>
        </tbody>
    </table>
</div>
<!-- /Dashboard Table -->