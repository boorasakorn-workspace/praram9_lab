<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MDL_APILab extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->db_lab = $this->load->database('db_lab', TRUE);
        $this->db_lab->reset_query();
    }

    function getDataApi($secure_code)
    {

        $get_head_secure_code = '';
        $get_head_tokenno = '';
        $get_head_hn = '';
        $get_head_nation = '';
        $result_format_info = '';
        $result_format_lab = '';


        $get_head = $this->db->select('*')
            ->from('vw_api_mobile_labxray')
            ->where('secure_code', $secure_code)->limit('1')->get();

        if ($get_head->num_rows() > 0) {
            $result_head =  $get_head->result_array();

            $get_head_secure_code = $result_head[0]['secure_code'];
            $get_head_tokenno = $result_head[0]['tokenno'];
            $get_head_hn = $result_head[0]['hn'];
            $get_head_nation = $result_head[0]['nation'];

            $get_vw_patient = $this->db->select('*')
                ->from('vw_api_mobile_labxray')
                ->where('secure_code', $result_head[0]['secure_code'])
                ->get()->result_array();

            $result_format_info = [];
            foreach ($get_vw_patient as $key => $value) {


                $cwhen_current = 'null';
                $text_current = 'null';

                if ($value['qclose'] != null) {
                    $text_current = 'Close';
                    $cwhen_current = date('Y-m-d H:i:s', strtotime($value['qclose']));
                } else if ($value['qcomplete'] != null) {
                    $text_current = 'Complete';
                    $cwhen_current = date('Y-m-d H:i:s', strtotime($value['qcomplete']));
                } else if ($value['qcancel'] != null) {
                    $text_current = 'Cancel';
                    $cwhen_current = date('Y-m-d H:i:s', strtotime($value['qcancel']));
                }


                $text1_call_hold = 'null';
                $time1_call_Hold = 'null';

                if ($value['callcwhen'] != null && $value['holdcwhen'] != null) {

                    $text1_call_hold = (strtotime($value['callcwhen']) >  strtotime($value['holdcwhen'])) ? 'Call' : 'Hold';
                    $time1_call_Hold = (strtotime($value['callcwhen']) >  strtotime($value['holdcwhen'])) ? date('Y-m-d H:i:s', strtotime($value['callcwhen'])) : date('Y-m-d H:i:s', strtotime($value['holdcwhen']));
                } else if ($value['callcwhen'] != null && $value['holdcwhen'] == null) {

                    $time1_call_Hold = date('Y-m-d H:i:s', strtotime($value['callcwhen']));
                    $text1_call_hold = 'Call';
                } else if ($value['callcwhen'] == null && $value['holdcwhen'] != null) {

                    $time1_call_Hold = date('Y-m-d H:i:s', strtotime($value['holdcwhen']));
                    $text1_call_hold = 'Hold';
                }

                $format_information = [
                    "vn" => $value['vn'],
                    "department_code" =>   $value['doctorcode'],
                    "department_name_th" =>   $value['doctorname'],
                    "department_name_en" =>   null,
                    "visit_start" =>   $value['visit_date'],
                    "send_doctor_status" =>   $value['patient_status'],
                    "send_doctor_status_time" =>   $value['visit_date'],
                    "doctor_code" =>   $value['doctorcode'],
                    "doctor_name_th" =>   $value['doctor_send_nameth'],
                    "doctor_name_en" =>  $value['doctor_send_nameen'],
                    "doctor_slot" =>   $value['doctor_slot_time'],
                    "doctor_room" =>   $value['doctor_room'],
                    "queue_waiting" =>   number_format($value['waittime']),
                    "queue_event_call_hold" =>   $text1_call_hold,
                    "queue_event_call_hold_time" =>   $time1_call_Hold,
                    "queue_call_room" =>   $value['queue_call_room'],
                    "queue_current_status" =>   $text_current,
                    "queue_current_status_time" => $cwhen_current,
                ];
                array_push($result_format_info, $format_information);
            }
        } else {
            $result_format_info = '';
        }

        $getvw_lab = $this->db_lab->select('*')
            ->from('vw_api_mobile_lab')
            ->where('secure', $secure_code)->order_by('uid', 'asc')->get();

        if ($getvw_lab->num_rows() > 0) {
            $result_pdlab = $getvw_lab->result_array();

            $result_format_lab = [];
            foreach ($result_pdlab as $valuelab) {

                $callhold_time = 'null';
                $text_call_hold = 'null';

                if ($valuelab['calltime'] != null && $valuelab['holdtime'] != null) {

                    $callhold_time  = (strtotime($valuelab['calltime']) > strtotime($valuelab['holdtime'])) ? date('Y-m-d H:i:s', strtotime($valuelab['calltime'])) : date('Y-m-d H:i:s', strtotime($valuelab['holdtime']));
                    $text_call_hold = (strtotime($valuelab['calltime']) > strtotime($valuelab['holdtime'])) ? 'Call' : 'Hold';
                } else if ($valuelab['calltime'] != null && $valuelab['holdtime'] == null) {

                    $callhold_time = date('Y-m-d H:i:s', strtotime($valuelab['calltime']));
                    $text_call_hold = 'Call';
                } else if ($valuelab['calltime'] == null && $valuelab['holdtime'] != null) {

                    $callhold_time = date('Y-m-d H:i:s', strtotime($valuelab['holdtime']));
                    $text_call_hold = 'Hold';
                }

                $currrent_text = ($valuelab['closetime'] == null) ? 'null' : 'Close';
                $current_time = 'null';
                if ($valuelab['closetime'] != null) {
                    $current_time = date('Y-m-d H:i:s', strtotime($valuelab['closetime']));
                }

                $format_pdlab = [
                    "patientdetailuid" => $valuelab['uid'],
                    "queueno" => $valuelab['queueno'],
                    "vn" => $valuelab['en'],
                    'send_time' => date('Y-m-d H:i:s', strtotime($valuelab['cwhen'])),
                    "waiting" => $valuelab['waiting'],
                    "call_hold" => $text_call_hold,
                    "call_hold_time" => $callhold_time,
                    "call_room" => $valuelab['calllist'],
                    "current_status" => $currrent_text,
                    "current_status_time" => $current_time,
                ];

                array_push($result_format_lab, $format_pdlab);
            }
        } else {
            $result_format_lab = false;
        }


        $result_api = [
            "secure" =>    $get_head_secure_code,
            "queueno" => $get_head_tokenno,
            "hn" =>     $get_head_hn,
            "language" => $get_head_nation,
            "infomation" => $result_format_info,
            'datalab' => $result_format_lab
        ];

        return $result_api; //$result_head //$result_api
    }

    function insertLab($data){
        $SearchHN = $data['hn'];
        $location = $data['location'];
        $queryStr = "
        SELECT *,'$location' as location FROM vw_getscanqueue 
        WHERE replace(hn,'-','') LIKE replace('$SearchHN','-','') 
        ORDER BY vn ASC 
        LIMIT 1
        ";
        $query = $this->db->query($queryStr);
        return $query->result();
    }

    function getWaitingHold($location){
        $this->db_lab->select('*')
                        ->from('vw_api_display_lab')
                        ->where('location',$location);
        $query = $this->db_lab->get();
        return $query->row();
    }
}

