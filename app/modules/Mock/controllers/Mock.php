<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mock extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get(){
		$json = '
		{
			"responseCode": 1,
			"responseMsg": "Success",
			"requestTime": "2020-05-18T07:39:39.6839477Z",
			"responseTime": "2020-05-18T07:39:39.6995951Z",
			"detail": [
				{
					"labRequestNo": "1621225184",
					"doctorCode": "A0984",
					"doctorNameTH": "นพ. วิศิษฐ์ ลิ่วลมไพศาล",
					"doctorNameEN": "WISIT LIWLOMPAISAN,M.D.",
					"hN": "718208",
					"vN": "A0001",
					"titleTH": "นาย ",
					"firstNameTH": "เมธ***",
					"lastNameTH": "รอด***",
					"titleEN": "Mr. ",
					"firstNameEN": "Mat***",
					"lastNameEN": "Rod***",
					"fromClinicCode": "2100",
					"fromClinicNameTH": "สถาบันโรคไตและเปลี่ยนไต",
					"fromClinicNameEN": "Kidney Disease And Transplantation Institute",
					"fromWardCode": null,
					"fromWardName": null,
					"payorCode": "011",
					"payorNameTH": "ทั่วไป (ไทย)",
					"payorNameEN": "GENERAL (THAI)",
					"item": [
						{
							"labCode": "1001",
							"labNameTH": "Sugar( fasting )  (32203)",
							"labNameEN": "Sugar( fasting )   (32203)",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "0001-01-01T00:00:00.0000000+07:00"
						},
						{
							"labCode": "1004",
							"labNameTH": "BUN   (32201)",
							"labNameEN": "BUN   (32201).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "2020-05-18T07:39:54.0000000+07:00"
						},
						{
							"labCode": "1005",
							"labNameTH": "Creatinine   (32202)",
							"labNameEN": "Creatinine   (32202).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "2020-05-18T07:39:54.0000000+07:00"
						},
						{
							"labCode": "3003",
							"labNameTH": "CBC + Platelet count   (30101)",
							"labNameEN": "CBC + Platelet count   (30101).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "0001-01-01T00:00:00.0000000+07:00"
						},
						{
							"labCode": "2044",
							"labNameTH": "Prograf (tacrolimus )   (33203)",
							"labNameEN": "Prograf (tacrolimus )   (33203).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "0001-01-01T00:00:00.0000000+07:00"
						},
						{
							"labCode": "1028",
							"labNameTH": "Hemoglobin A1c****   (32401)",
							"labNameEN": "Hemoglobin A1c****   (32401).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "0001-01-01T00:00:00.0000000+07:00"
						},
						{
							"labCode": "1008",
							"labNameTH": "HDL Cholesterol   (32503)",
							"labNameEN": "HDL Cholesterol   (32503).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "0001-01-01T00:00:00.0000000+07:00"
						},
						{
							"labCode": "1010",
							"labNameTH": "LDL Cholesterol   (32504)",
							"labNameEN": "LDL Cholesterol   (32504).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "0001-01-01T00:00:00.0000000+07:00"
						},
						{
							"labCode": "4016",
							"labNameTH": "Urine Exam.   (31001)",
							"labNameEN": "Urine Exam.   (31001).",
							"cxlReasonCode": null,
							"cxlReasonName": null,
							"voidDateTime": "2020-05-18T07:39:54.0000000+07:00"
						}
					],
					"specimenReceiveDateTime": "2020-05-18T07:52:10.0000000+07:00",
					"labStatus": "Resulted",
					"remarksMemo": "เท่าเดิม",
					"cxlReasonCode": null,
					"cxlReasonName": null,
					"cxlDateTime": "0001-01-01T00:00:00.0000000+07:00"
				}
			]
		}
		';
		echo $json;
	}

	public function decode(){
		$ch1 = curl_init();
		curl_setopt($ch1, CURLOPT_URL,base_url('mock/get'));
		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch1);
		curl_close($ch1);
		return json_decode($output,TRUE);
	}

	public function print(){
		$decode = $this->decode();
		$detail = $decode['detail'][0];
		echo '<pre>'.var_export($detail,TRUE).'</pre>';
	}
}
