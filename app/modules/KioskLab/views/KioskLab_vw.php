<div class="" style="width: 100%; height:100%;">

    <div class="row no-gutters center head bg_color" style="height: 5%;background-color:#008387">
        <!-- ยินดีต้อนรับ -->
    </div>

    <div class="row no-gutters col-12  center border_lr" style="height: 90%;">

        <div class="col-12 center" style="position: relative; height: 100%;">
            <?= single_img('img/bg_image_url.jpg', array('class' => 'bg_img')) ?>
            <input class="inp" style="position: absolute; outline: none;" placeholder="Scan QR Code / กรอก HN" id="scan_prescription">


        </div>

    </div>

    <div class="row no-gutters center footer bg_color" style="height: 5%;display:block;background-color:#78bfaf">

        <!-- Modal พิมซ้ำ-->
        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
            WEL 1000 COME
        </button> -->
        <span style="float:left;color:#b9bdc1;">.</span>
    </div>


</div>

<!-- Modal re print -->
<div class=" modal" id="md_scan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="text-align:center;">
    <div class="modal-dialog modal_box modal-dialog-centered center modal-lg" role="document">
        <div class="modal-content" style="width: fit-content;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="font-size:3rem;">&times;</span>
                </button>
            </div>

            <div class="modal-body row no-gutters col-12" id='md_cont'>
            </div>

            <!-- <div class="row no-gutters justify-content-around modal-footer">
                <button type="button" class="col-2 btn btn_pri btn-secondary" style="margin-left: 80px;" data-dismiss="modal">พิมพ์บัตรคิวซ้ำ</button>
                <button type="button" data-dismiss="modal" class="col-2 btn btn_pri btn-primary" style="margin-right: 80px;">ปิด</button>
            </div> -->

            <div class="row no-gutters justify-content-around modal-footer" id='md_btn'>
                <!-- <button type="button" data-dismiss="modal" class="col-2 btn btn_pri btn-primary" style="">ปิด</button> -->
            </div>

        </div>
    </div>
</div>