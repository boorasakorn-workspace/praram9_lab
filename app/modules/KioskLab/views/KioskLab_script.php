<script>
    var KioskLocation = "<?=$this->session->userdata('location_kiosklab');?>";

    function reverse(str) {
        return str.split('').reverse().join('');
    }
    String.prototype.reverseInsert = function (index, string) {
        let thisString = this;
        thisString = reverse(thisString);
        if (index > 1)
            return reverse(thisString.substring(0, index) + string + thisString.substring(index, thisString.length));
        
        return string + thisString;        
    };

    function ScanInput(Input){
        console.log(Input);
        if (Input == '') {
            swal({
                title: "กรุณากรอก HN",
                type: "error",
                confirmButtonClass: "btn-danger",  
                confirmButtonText: "Close",
                timer: 1000
            });
        }else{
            let Input_trim = Input.trim();
            Input_trim = Input_trim.replace(/^-+/i, '');
            console.log(Input_trim);
            swal({
              title: "กำลังค้นหาHN",
              text: "กรุณารอสักครู่",
              imageUrl: "<?=base_url('static/img/loading/loading_1.gif');?>",
              showConfirmButton: false,
              allowOutsideClick: false
            });
            var PostURL = "<?=base_url('Lab/SearchPatient');?>";
            var PostData = {
                    'hn': Input_trim,
                    'locationuid': KioskLocation
            };
            $.post(PostURL, PostData)
                .done(async function(response){                
                    console.log(response);
                    if(response['message']['result'] == 'Success' || response['message']['result'] == true ){
                        await swal.close();
                        let patient_data = response['message']['data'] != undefined ? response['message']['data'] : (response['message']['dbdata'] != undefined ? response['message']['dbdata'] : '');
                        var ViewURL = "<?=base_url('KioskLab/QueueModal');?>";
                        var ViewData = {
                            'queuedata' : patient_data//( response['message']['data'] != undefined ?  response['message']['data'] : '' ),
                        };
                        await $.post(ViewURL, ViewData)
                            .done(function(view_response){
                                if( $('#md_scan').length && view_response['message']['html'] != undefined ){
                                    $('#md_scan .modal-body').html(view_response['message']['html']);
                                    $('#md_scan').modal('show');
                                }
                            });
                        await $("#scan_prescription").val('');
                    }else{
                        var PostLabURL = "<?=base_url('Lab/ScanPatient');?>";
                        var PostLabData = {
                                'hn': Input_trim,
                                'locationuid': KioskLocation
                        };
                        await $.post(PostLabURL, PostLabData)
                            .done(async function(lab_response){
                                if(lab_response['message']['queuedata']){
                                    await swal.close();                                    
                                    var ViewURL = "<?=base_url('KioskLab/QueueModal');?>";
                                    var ViewData = {
                                        'labdata' : true,
                                        'queuedata' : lab_response['message']['queuedata']
                                    };
                                    await $.post(ViewURL, ViewData)
                                        .done(function(view_response){
                                            if( $('#md_scan').length && view_response['message']['html'] != undefined ){
                                                $('#md_scan .modal-body').html(view_response['message']['html']);
                                                $('#md_scan').modal('show');
                                            }
                                        });

                                }else{
                                    await swal({
                                        title: "ไม่พบ HN นี้ในระบบ",
                                        type: "error",
                                        confirmButtonClass: "btn-danger",  
                                        confirmButtonText: "Close",
                                        timer: 1000
                                    });
                                }
                            })
                            .fail(function(){
                                swal({
                                    title: "Error",
                                    type: "error",
                                    confirmButtonClass: "btn-danger",  
                                    confirmButtonText: "Close",
                                    timer: 1000
                                });
                                $("#scan_prescription").val('');
                            })
                            .always(function(){
                                $("#scan_prescription").val('');
                            })                        
                    }
                })
                .fail(function(){
                    swal({
                        title: "Error",
                        type: "error",
                        confirmButtonClass: "btn-danger",  
                        confirmButtonText: "Close",
                        timer: 1000
                    });
                    $("#scan_prescription").val('');
                });
        }
    }

    $(document).ready(function() {

        $(document).on('hidden.bs.modal','#md_scan',function(){
            $('#md_scan .modal-body').html('');
            $("#scan_prescription").val('');
        });

        $(document).on('click','[data-insertpatient]',async function(){
            var Print = ($(this).data('insertpatient') == 'print' ? true : false);
            var PostURL = "<?=base_url('Lab/InsertPatient/');?>";
            //var PostURL = "<?=base_url('Lab/API/Scan/FALSE');?>";
            var PostData = {
                    'hn': $(this).data('hn'),
                    'vn': $(this).data('vn'),
                    'queueno': $(this).data('queueno'),
                    'patienttype': $('input[name="patienttype"]:checked').val(),
                    'locationuid':KioskLocation
            };
            await $.post(PostURL, PostData)
                .done(function(response){
                    console.log(response)
                    if(Print){
                        swal({
                            title: "กำลังพิมพ์",
                            text: "กรุณารอสักครู่",
                            imageUrl: "<?=base_url('static/img/loading/loading_1.gif');?>",
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        var PrintURL = "<?=base_url('KioskLab/PrintQueue');?>";
                        console.log("Print Queue");
                        $.post(PrintURL, PostData)
                            .done(function(print_response){
                                console.log(print_response);
                                if(!print_response['message']['result']){
                                    swal({
                                        title: "พิมพ์ไม่สำเร็จ",
                                        type: "error",
                                        confirmButtonClass: "btn-danger",  
                                        confirmButtonText: "Close",
                                        timer: 1000
                                    });
                                }else{                                
                                    swal.close();
                                }
                            })
                            .fail(function(){
                                swal({
                                    title: "พิมพ์ไม่สำเร็จ",
                                    type: "error",
                                    confirmButtonClass: "btn-danger",  
                                    confirmButtonText: "Close",
                                    timer: 1000
                                });
                            });
                    }
                })
                .fail(function(){
                    swal({
                        title: "Error",
                        type: "error",
                        confirmButtonClass: "btn-danger",  
                        confirmButtonText: "Close",
                        timer: 1000
                    });
                });
            await $(this).closest('.modal').modal('hide');
        })

        $(document).on('click','[data-reinsertpatient]',async function(){
            var Print = ($(this).data('reinsertpatient') == 'print' ? true : false);
            var PostURL = "<?=base_url('Lab/InsertPatient/');?>";
            //var PostURL = "<?=base_url('Lab/API/Scan/FALSE');?>";
            var PostData = {
                    'hn': $(this).data('hn'),
                    'vn': $(this).data('vn'),
                    'queueno': $(this).data('queueno'),
                    'locationuid':KioskLocation
            };
            await $.post(PostURL, PostData)
                .done(function(response){
                    console.log(response)
                    if(Print){
                        swal({
                            title: "กำลังพิมพ์",
                            text: "กรุณารอสักครู่",
                            imageUrl: "<?=base_url('static/img/loading/loading_1.gif');?>",
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        var PrintURL = "<?=base_url('KioskLab/PrintQueue');?>";
                        console.log("Print Queue");
                        $.post(PrintURL, PostData)
                            .done(function(print_response){
                                console.log(print_response);
                                if(!print_response['message']['result']){
                                    swal({
                                        title: "พิมพ์ไม่สำเร็จ",
                                        type: "error",
                                        confirmButtonClass: "btn-danger",  
                                        confirmButtonText: "Close",
                                        timer: 1000
                                    });
                                }else{                                
                                    swal.close();
                                }
                            })
                            .fail(function(){
                                swal({
                                    title: "พิมพ์ไม่สำเร็จ",
                                    type: "error",
                                    confirmButtonClass: "btn-danger",  
                                    confirmButtonText: "Close",
                                    timer: 1000
                                });
                            });
                    }
                })
                .fail(function(){
                    swal({
                        title: "Error",
                        type: "error",
                        confirmButtonClass: "btn-danger",  
                        confirmButtonText: "Close",
                        timer: 1000
                    });
                });
            await $(this).closest('.modal').modal('hide');
        })

        $(document).on('click','[printpatient]',async function(){
            swal({
                title: "กำลังพิมพ์",
                text: "กรุณารอสักครู่",
                imageUrl: "<?=base_url('static/img/loading/loading_1.gif');?>",
                showConfirmButton: false,
                allowOutsideClick: false,
            });
            var PrintURL = "<?=base_url('KioskLab/PrintQueue');?>";
            var PostData = {
                    'hn': $(this).data('hn'),
                    'vn': $(this).data('vn'),
                    'queueno': $(this).data('queueno'),
            };
            console.log("RePrint Queue");
            await $.post(PrintURL, PostData)
                .done(function(print_response){
                    console.log(print_response);
                    if(!print_response['message']['result']){
                        swal({
                            title: "พิมพ์ไม่สำเร็จ",
                            type: "error",
                            confirmButtonClass: "btn-danger",  
                            confirmButtonText: "Close",
                            timer: 1000
                        });
                    }else{                                
                        swal.close();
                    }
                })
                .fail(function(){
                    swal({
                        title: "พิมพ์ไม่สำเร็จ",
                        type: "error",
                        confirmButtonClass: "btn-danger",  
                        confirmButtonText: "Close",
                        timer: 1000
                    });
                });
            await $(this).closest('.modal').modal('hide');
        });

        $(document).keyup(function(e){
            if (e.keyCode == 13) {
                ScanInput($('#scan_prescription').val());
            }else if(e.keyCode == 8){
                let Value = $('#scan_prescription').val().slice(0,-1);
                $('#scan_prescription').val(Value);
            }else if(e.keyCode == 189){
                let Value = $('#scan_prescription').val();
                $('#scan_prescription').val(Value+"-");
            }else if(e.keyCode == 191){
                let Value = $('#scan_prescription').val();
                $('#scan_prescription').val(Value+"/");
            }else if(e.keyCode >= 48 && e.keyCode <= 57){
                let keyChar = String.fromCharCode(e.keyCode);
                let Value = $('#scan_prescription').val();
                $('#scan_prescription').val( Value+keyChar );
            }
            // else if(e.keyCode != 32 && e.keyCode != 46 && !(e.keyCode >= 65 && e.keyCode <= 90) && !(e.keyCode >= 97 && e.keyCode <= 122) ){
            //     let keyChar = String.fromCharCode(e.keyCode);
            //     let Value = $('#scan_prescription').val();
            //     $('#scan_prescription').val( Value+keyChar );
            // }
        })
        $("#scan_prescription").keyup(function(e) {
            if(e.keyCode == 8 || e.keyCode == 32){
                return false;
            }
        });
        $("#scan_prescription").keypress(function(e) {
            return false;
        });
    });
</script>