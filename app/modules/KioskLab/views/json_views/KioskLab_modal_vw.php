<style>
    .modal-header .close {
        padding: 1rem 1rem;
        margin: -1.5rem -1rem -1rem auto;
    }
</style>
<div style="color: #008387;font-size: 3rem;font-weight:500;min-width: 50rem;">
    <?php foreach ($Data['queuedata'] as $QD_Key => $QD_Value) { ?>
        <span>หมายเลขบริการของคุณ</span>
        <?php if (!isset($QD_Value['patientdetail']) || !is_array($QD_Value['patientdetail'])) { ?>
            <span> <?= $QD_Value['tokenno']; ?></span>
            <p>ต้องการพิมพ์บัตรคิวหรือไม่</p>
            
            <div style="text-align: center;display:none;">
                <label class="radio-inline" style="margin: 0 1.5rem;"><input type="radio" style="border: 0px;width: 2rem;height: 2rem;" name="patienttype" value="0" checked=""><?= assets_img("img/patient_icon/0.png", 'height=40px;width:40px;'); ?></label>
                <label class="radio-inline" style="margin: 0 1.5rem;"><input type="radio" style="border: 0px;width: 2rem;height: 2rem;" name="patienttype" value="1"><?= assets_img("img/patient_icon/1.png", 'height=40px;width:40px;'); ?></label>
                <label class="radio-inline" style="margin: 0 1.5rem;"><input type="radio" style="border: 0px;width: 2rem;height: 2rem;" name="patienttype" value="2"><?= assets_img("img/patient_icon/2.png", 'height=40px;width:40px;'); ?></label>
                <label class="radio-inline" style="margin: 0 1.5rem;"><input type="radio" style="border: 0px;width: 2rem;height: 2rem;" name="patienttype" value="3"><?= assets_img("img/patient_icon/3.png", 'height=40px;width:40px;'); ?></label>
            </div>
           
            <button type="button" class="btn col-4 btn_print" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="print" data-hn="<?= $QD_Value['hn']; ?>" data-vn="<?= $QD_Value['vn']; ?>" data-queueno="<?= $QD_Value['tokenno']; ?>">พิมพ์บัตรคิว</button>
            <button type="button" class="btn col-4 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="insert" data-hn="<?= $QD_Value['hn']; ?>" data-vn="<?= $QD_Value['vn']; ?>" data-queueno="<?= $QD_Value['tokenno']; ?>">บันทึก</button>
        <?php } else { ?>
            <span> <?= $QD_Value['patientdetail']['queueno']; ?></span>
            <?php if (isset($Data['labdata']) && $Data['labdata']) { ?>
                <?php if ($QD_Value['patientdetail']['closed_queue'] != NULL) { ?>
                    <p style="color: #FF0000">คิวเสร็จสิ้นแล้ว</p>
                    <?php if ($QD_Value['patientdetail']['location'] != NULL) { ?>
                        <button type="button" class="btn col-4 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
                    <?php } else { ?>
                        <p>ต้องการออกคิวใหม่หรือไม่</p>
                        <button type="button" class="btn col-3 btn_print" style="font-size: 2.2rem;min-width: fit-content;" data-reinsertpatient="print" data-hn="<?= $QD_Value['patientdetail']['hn']; ?>" data-vn="<?= $QD_Value['patientdetail']['en']; ?>" data-queueno="<?= $QD_Value['patientdetail']['queueno']; ?>">พิมพ์บัตรคิว</button>
                        <button type="button" class="btn col-3 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-reinsertpatient="insert" data-hn="<?= $QD_Value['patientdetail']['hn']; ?>" data-vn="<?= $QD_Value['patientdetail']['en']; ?>" data-queueno="<?= $QD_Value['patientdetail']['queueno']; ?>">บันทึกคิวใหม่</button>
                        <button type="button" class="btn col-3 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ไม่ต้องการ</button>
                    <?php } ?>
                <?php } else { ?>
                    <p>คิวอยู่ในระบบ</p>
                    <p>ต้องการพิมพ์บัตรคิวหรือไม่</p>
                    <button type="button" class="btn col-4 btn_print" style="font-size: 2.2rem;min-width: fit-content;" printpatient data-hn="<?= $QD_Value['patientdetail']['hn']; ?>" data-vn="<?= $QD_Value['patientdetail']['en']; ?>" data-queueno="<?= $QD_Value['patientdetail']['queueno']; ?>">พิมพ์อีกครั้ง</button>
                    <button type="button" class="btn col-4 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
                <?php } ?>
            <?php } else { ?>
                <?php if ($QD_Value['patientdetail']['closed_queue'] != NULL) { ?>
                    <p style="color: #FF0000">คิวเสร็จสิ้นแล้ว</p>
                    <p>ต้องการออกคิวใหม่หรือไม่</p>
                    <button type="button" class="btn col-3 btn_print" style="font-size: 2.2rem;min-width: fit-content;" data-reinsertpatient="print" data-hn="<?= $QD_Value['patientdetail']['hn']; ?>" data-vn="<?= $QD_Value['patientdetail']['en']; ?>" data-queueno="<?= $QD_Value['patientdetail']['queueno']; ?>">พิมพ์บัตรคิว</button>
                    <button type="button" class="btn col-3 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-reinsertpatient="insert" data-hn="<?= $QD_Value['patientdetail']['hn']; ?>" data-vn="<?= $QD_Value['patientdetail']['en']; ?>" data-queueno="<?= $QD_Value['patientdetail']['queueno']; ?>">บันทึกคิวใหม่</button>
                    <button type="button" class="btn col-3 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ไม่ต้องการ</button>
                <?php } else { ?>
                    <p>คิวยังคงอยู่ในระบบ</p>
                    <p>ต้องการพิมพ์บัตรคิวอีกครั้งหรือไม่</p>
                    <button type="button" class="btn col-4 btn_print" style="font-size: 2.2rem;min-width: fit-content;" printpatient data-hn="<?= $QD_Value['patientdetail']['hn']; ?>" data-vn="<?= $QD_Value['patientdetail']['en']; ?>" data-queueno="<?= $QD_Value['patientdetail']['queueno']; ?>">พิมพ์อีกครั้ง</button>
                    <button type="button" class="btn col-4 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>
</div>

<?php /*
<div style="color: #3579B6;font-size: 3rem;font-weight:500;">
    <?php if( isset($Data['queuedata']) && $Data['queuedata'] ){ ?>
        <table class="table table-striped table-bordered">
            <thead>
                <th>Queueno</th>
                <th>HN</th>
                <th>VN</th>
                <th>Status</th>
                <th>Print</th>
                <th>Enter</th>
            </thead>
            <tbody>
                <?php foreach($Data['queuedata'] as $QD_Key => $QD_Value){ ?>
                    <tr>
                        <td><?=$QD_Value['tokenno'];?></td>
                        <td><?=$QD_Value['hn'];?></td>
                        <td><?=$QD_Value['vn'];?></td>
                        <td><?=( (isset($QD_Value['patientdetail']['closed_queue']) && $QD_Value['patientdetail']['closed_queue'] ) ? 'คิวเสร็จสิ้นแล้ว' : ( isset($QD_Value['patientdetail']) && is_array($QD_Value['patientdetail']) ? 'คิวยังคงอยู่ในระบบ' : 'คิวไม่อยู่ในระบบ'));?></td>
                        <td>
                            <?php if( !isset($QD_Value['patientdetail']) || !is_array($QD_Value['patientdetail']) ){ ?>
                                <button type="button" class="btn col-4 btn_print" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="print" data-hn="<?=$QD_Value['hn'];?>" data-vn="<?=$QD_Value['vn'];?>" data-queueno="<?=$QD_Value['tokenno'];?>">พิมพ์บัตรคิว</button>
                            <?php }else if( isset($QD_Value['patientdetail']) && $QD_Value['patientdetail']['closed_queue'] == NULL ){?>
                                <button type="button" class="btn col-4 btn_print" style="font-size: 2.2rem;min-width: fit-content;" printpatient data-hn="<?=$QD_Value['hn'];?>" data-vn="<?=$QD_Value['vn'];?>" data-queueno="<?=$QD_Value['tokenno'];?>">พิมพ์อีกครั้ง</button>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if( !isset($QD_Value['patientdetail']) || !is_array($QD_Value['patientdetail']) ){?>
                                <button type="button" class="btn col-4 btn_save" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="insert" data-hn="<?=$QD_Value['hn'];?>" data-vn="<?=$QD_Value['vn'];?>" data-queueno="<?=$QD_Value['tokenno'];?>">บันทึก</button>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>
</div>
*/ ?>

<?php /*
<div style="color: #3579B6;font-size: 3rem;font-weight:500;">
<?php if( isset($Data['queuedata']) && $Data['queuedata'] ){ ?>
    <?php foreach($Data['queuedata'] as $QD_Key => $QD_Value){ ?>
        <p><?=$QD_Value['tokenno'];?></p>
        <?php if( isset($QD_Value['patientdetail']) && $QD_Value['patientdetail'] ): ?>
            <?php if( $QD_Value['patientdetail']['closed_queue'] != NULL){ ?>
                <!-- <p style="color: #FF0000">คิวเสร็จสิ้นแล้ว</p> -->
                <button type="button" class="btn col-4 btn_pri btn-primary" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
            <?php }else{ ?>
                <!-- <p>คิวยังทำงานอยู่</p> -->
                <p>ต้องการพิมพ์บัตรคิวอีกครั้งหรือไม่</p>
                <button type="button" class="btn col-4 btn_pri btn-secondary" style="font-size: 2.2rem;min-width: fit-content;" printpatient data-hn="<?=$QD_Value['hn'];?>" data-vn="<?=$QD_Value['vn'];?>" data-queueno="<?=$QD_Value['tokenno'];?>">พิมพ์บัตรคิว</button>
                <button type="button" class="btn col-4 btn_pri btn-primary" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
            <?php } ?>
        <?php ;else: ?>
            <p>ต้องการพิมพ์บัตรคิวหรือไม่</p>
            <button type="button" class="btn col-4 btn_pri btn-secondary" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="print" data-hn="<?=$QD_Value['hn'];?>" data-vn="<?=$QD_Value['vn'];?>" data-queueno="<?=$QD_Value['tokenno'];?>">พิมพ์บัตรคิว</button>
            <button type="button" class="btn col-4 btn_pri btn-primary" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="insert" data-hn="<?=$QD_Value['hn'];?>" data-vn="<?=$QD_Value['vn'];?>" data-queueno="<?=$QD_Value['tokenno'];?>">ปิด</button>
        <?php endif; ?>
    <?php } ?>
<?php } ?>
</div>
*/ ?>



<?php /*
<?php if( isset($Data['patientdetail']) && $Data['patientdetail'] ){ ?>
    <div style="color: #3579B6;font-size: 3rem;font-weight:500;">
        <p>หมายเลขบริการของคุณ</p>
        <p><?=$Data['queueno'];?></p>
        <?php if( $Data['patientdetail']['closed_queue'] ){ ?>
            <p style="color: #FF0000">คิวเสร็จสิ้นแล้ว</p>
            <button type="button" class="btn col-4 btn_pri btn-primary" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
        <?php }else{ ?>
            <p>คิวยังทำงานอยู่</p>
            <p>ต้องการพิมพ์บัตรคิวอีกครั้งหรือไม่</p>
            <button type="button" class="btn col-4 btn_pri btn-secondary" style="font-size: 2.2rem;min-width: fit-content;" printpatient data-hn="<?=$Data['hn'];?>" data-vn="<?=$Data['vn'];?>" data-queueno="<?=$Data['queueno'];?>">พิมพ์บัตรคิว</button>
            <button type="button" class="btn col-4 btn_pri btn-primary" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
        <?php } ?>        
    </div>
<?php }else if( isset($Data['queueno']) && $Data['queueno'] ){ ?>
    <div style="color: #3579B6;font-size: 3rem;font-weight:500;">
        <p>หมายเลขบริการของคุณ</p>
        <p><?=$Data['queueno'];?></p>
        <p>ต้องการพิมพ์บัตรคิวหรือไม่</p>
        <button type="button" class="btn col-4 btn_pri btn-secondary" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="print" data-hn="<?=$Data['hn'];?>" data-vn="<?=$Data['vn'];?>" data-queueno="<?=$Data['queueno'];?>">พิมพ์บัตรคิว</button>
        <button type="button" class="btn col-4 btn_pri btn-primary" style="font-size: 2.2rem;min-width: fit-content;" data-insertpatient="insert" data-hn="<?=$Data['hn'];?>" data-vn="<?=$Data['vn'];?>" data-queueno="<?=$Data['queueno'];?>">ปิด</button>
    </div>
<?php }else{ ?>
    <div style="color: #FF0000;font-size: 3rem;font-weight:500;">
        <p>ไม่พบข้อมูล กรุณาติดต่อเจ้าหน้าที่</p>
        <button type="button" class="btn col-4 btn_pri btn-primary" style="font-size: 2.2rem;min-width: fit-content;" data-dismiss="modal">ปิด</button>
    </div>
<?php } ?>
*/ ?>