<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KioskLab extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('json');
		$this->load->helper('recursiveconvert');
		$this->load->helper('calculate');
    }

    function index(){
        if( !$this->session->userdata('location_kiosklab') ){
            redirect('KioskLab/Location');
        }else{
            $Location = $this->session->userdata('location_kiosklab');
            redirect('KioskLab/KioskMain/'.$Location);
        }
    }

	private function renderView($Data){
		$this->load->module('Template_Module');
		$Data['Module'] = 'KioskLab';
		$this->template_module->Template('Kiosk_tem', $Data);
    }

    function Location($Location = NULL){
        if(!$Location){
            $all_location = to_stdClass(array(
                array(
                    'id' => '1',
                    'name' => 'ชั้น 1',
                ),
                array(
                    'id' => '2',
                    'name' => 'ชั้น 2',
                ),
                array(
                    'id' => '3',
                    'name' => 'ชั้น 3',
                ),
            ));
            echo single_css('vendor/bootstrap-4.4.1/css/bootstrap.min.css');
            echo "LOCATION SETUP"."<br>";
            if( $this->session->userdata('location_kiosklab') ) echo 'Current Location : '.$this->session->userdata('location_kiosklab')."<br>";
            foreach($all_location as $key => $value):
                echo '<a href="'.base_url('KioskLab/Location/'.$value->id).'">'.
                     '<button class="btn btn-primary btn-md">'.$value->name.'</button>'.
                     '</a> ';
            endforeach;
        }else{
            $this->session->set_userdata('location_kiosklab', $Location);
            redirect('KioskLab/index');
        }
    }
    
	public function KioskMain($Location = NULL){
        if( $Location == NULL && !$this->session->userdata('location_kiosklab') ){
            redirect('KioskLab/Location');
        }else if( $Location != NULL ){
            $this->session->set_userdata('location_kiosklab', $Location);
        }else{
            $this->index();
        }
		$Template = array(
			'Site_Title' => 'Kiosk Lab',
			'Content' => array(
                'Main' => 'KioskLab_vw',
            ),
			'Script' => array(
				'Script' => 'KioskLab_script',
			),
			'viewCSS' => array(
				'css' => 'KioskLab_style',
			),
		);
		$this->renderView($Template);
    }
    
    public function QueueModal($Data = NULL){
        $InputData = ($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array()));        
        $this->load->module('Lab');
        foreach($InputData['queuedata'] as $Q_Key => $Q_Value){
            $InputData['queuedata'][$Q_Key]['patientdetail'] = $this->lab->PatientStatus_HNLocation($Q_Value['hn'],$Q_Value['locationuid'],FALSE);
        }
        $view = 'KioskLab/json_views/KioskLab_modal_vw';
        $viewsData = array(
            'Data' => to_Array($InputData),
        );
        header( "Content-Type: application/json" );
        $message = array(
            'result' => 'Success',
            'text' => 'Get View Successful',
            'queuedata' => $InputData,
            'html' => $this->load->view($view,$viewsData,TRUE),
        );
        echo json_response(200, $message);
    }

    public function PrintQueue($Data = NULL,$JSONResponse = TRUE){
        $InputData = to_stdClass($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array()));
        //Get Queuedata
        $this->load->module('Lab');
        $QueueData = $this->lab->QueueInfo_Queueno($InputData->queueno,FALSE);
        if($QueueData){
            //Post Variable
            $queue = $QueueData->queueno;
            $when = date("Y-m-d H:i:s");
            $hn = $QueueData->hn;
            $vn = $QueueData->en;
            $language = ( !isset($QueueData->nation) || $QueueData->nation == 'TH' || $QueueData->nation == 'ไทย' ? 'TH' : 'EN');
            $secure = ( isset($QueueData->secure) && $QueueData->secure ? $QueueData->secure : '');
            $location = 'LAB';
            $sub_location = 0;

            $print_trigger = APIPRINT;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $print_trigger);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                "Language=$language&Queue=$queue&When=$when&HN=$hn&VN=$vn&Secure=$secure&location=$location&sub_location=$sub_location"
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $cURL = curl_exec($ch);
            curl_close($ch);
            if($JSONResponse){
                $message = array(
                    'method' => 'Print Queue',
                    'result' => true,
                    'trigger' => $cURL,
                    'input' => $InputData,
                );
                echo json_response(200, $message);				
            }else{
                return true;
            }
        }else{
            if($JSONResponse){
                $message = array(
                    'method' => 'Print Queue',
                    'result' => false,
                );
                echo json_response(200, $message);				
            }else{
                return false;
            }
        }
    }
        
    
}