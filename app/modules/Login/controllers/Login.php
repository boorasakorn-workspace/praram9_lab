<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('assets');
		$this->load->model('Login_md');
	}

	public function logintemplate($page, $title, $script)
	{
		$this->load->module('Template_Module');

		$Template = array(
			'Module' => 'Login',
			'Site_Title' => $title,
			'Content' => $page,
			'Script' => array(
				'Script' => $script,
			),
			'viewCSS' => array(
				'css' => 'Login_style',
			),
		);
		$this->template_module->Template('login_tem', $Template);
	}

	public function login_main()
	{
		$page = array('Main' => 'Login_vw');
		$title = 'Login';
		$script = 'Login_script';
		$this->logintemplate($page, $title, $script);
	}

	public function create_location($dataget)
	{
		$data = $dataget;

		$this->session->set_userdata('userlogin', $data);
	}


	public function login_check()
	{

		$result = $this->Login_md->login_md();

		if ($result != false) {

			$result_ses =  $this->create_location($result[0]);

			$data = array('status' => true, 'user_roleuid' => $result[0]['user_roleuid'], 'locationuid' => $result[0]['locationuid'], 'session' => $this->session->userdata('userlogin'));
			echo json_encode($data);
			
		} else {
			$data = array('status' => false);
			echo json_encode($data);
		}
	}
}
