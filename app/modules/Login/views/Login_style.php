<style>
    @font-face {
        font-family: 'nunito';
        src: url('<?= base_url('static/font/Nunito_Sans/' . 'NunitoSans-Regular.ttf'); ?>');
    }

    body {
        font-family: 'nunito';
        font-size: 15px;
    }

    .input-group-text {
        background-color: #ffffff !important;
        color: #0078bc !important;
    }

    .input-group>.custom-file,
    .input-group>.custom-select,
    .input-group>.form-control {
        position: relative;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        width: 1%;
        margin-bottom: 0;
    }

    .form-control {
        display: block;
        width: 100%;
        height: calc(2.21rem + 17px);
        padding: 0.375rem 1.4rem;
        font-size: 1.6rem;
        line-height: 1.5;
        color: #495057 !important;
        ;
        background-color: #fff !important;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    .input-group-text {
        padding: 0.375rem 1.263rem;
    }

    label {
        margin-bottom: .9rem;
    }

    .mb-3,
    .my-3 {
        margin-bottom: 1.6rem !important;
    }

    button,
    input,
    optgroup,
    select,
    textarea {
        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }

    .btn {
        font-size: 1.65rem;
        padding: .7rem 1.3rem;
        border-radius: 0.3rem;
    }
</style>