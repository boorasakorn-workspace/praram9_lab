<div class="container">
    <div class="col-lg-5 col-md-12 col-12 mx-auto" style="    border: 1px solid #f5f4f4;
		background: rgba(255,255 ,255 ,0.25);
		padding: 5px 38px;
		margin-top: 20px;
		box-shadow: 1px 3px 12px 7px #fdfdfd;
		border-radius: 4px;">
        <form role="form" method="post" action="<?php echo base_url('User/login'); ?>">
            <?php
            $success_msg = $this->session->flashdata('success_msg');
            $error_msg = $this->session->flashdata('error_msg');
            if ($success_msg) {
            ?>
                <div class="alert alert-success">
                    <?php echo $success_msg; ?>
                </div>
            <?php
            }
            if ($error_msg) {
            ?>
                <div class="alert alert-warning text-center">
                    <?php echo $error_msg; ?>
                </div>
            <?php
            } ?>
            <div class="text-center">
                <?= single_img('img/logo.png', array('style' => 'width: 150px;')) ?>
            </div>
            <div class="text-center" style="font-size: 30px;"> Q Management</div>
            <label>Username</label>
            <div class="input-group mb-3">

                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">
                        <i class="fa fa-user-o" style="font-size:24px"></i>
                    </span>
                </div>
                <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="required">
            </div>
            
            <label>Password</label>
            <div class="input-group mb-3">

                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">
                        <i class="fa fa-key" style="font-size:24px"></i>
                    </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="required" style="background-color: #fff;">
            </div>

            <div class="form-group text-center">
                <button class="btn btn-warning text-white" type="button" id="login" style="border-radius: .3rem;color: #ffffff;border-color: #008387;background-color: #008387;"><i class="fa fa-expeditedssl" style="font-size:24px"></i> Login</button>
                <div class="form-group">
                    <span style="font-size:10px; float:right;">v1.0.0</span>
                </div>
            </div>

        </form>
    </div>
</div>