<script>
    console.log(<?= json_encode($this->session->userdata('userlogin')) ?>, 'userlogin');

    $(document).ready(function() {

        var base_url = "<?= base_url() ?>";

        $("#username").keyup(function(e) {
            if (e.keyCode == 13) {
                login();
            }
        });

        $("#password").keyup(function(e) {
            if (e.keyCode == 13) {
                login();
            }
        });

        $("#login").click(function() {
            login();
        });

        function login() {

            if ($("#username").val() == '') {
                alert('กรุณากรอก username');
                return false;
            } else if ($("#password").val() == '') {
                alert('กรุณากรอก password');
                return false;
            }


            $.ajax({
                url: base_url + "Login/login_check",
                type: "post",
                data: {
                    username: $("#username").val(),
                    password: $("#password").val()
                },
                success: function(data) {
                    console.log(JSON.parse(data));
                    var data = JSON.parse(data);

                    if (data['status'] != false) {
                        if (data['session']['locationuser'] == 'SSP') {
                            window.location.href = base_url + 'labssp/lab_main';
                        }else{
                            window.location.href = base_url + 'lab/lab_main';
                        }
                    } else {
                        alert('username หรือ password ไม่ถูกต้อง');
                    }
                }
            });
        }

    });
</script>