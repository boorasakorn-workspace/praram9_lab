<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_LabSSP extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->db_lab = $this->load->database('db_lab', TRUE);
        $this->db_lab->reset_query();
    }

    function DBSelect($Table,$Where = NULL){
        $this->db_lab->select('*');
        $this->db_lab->from($Table);
        if(isset($Where) && $Where) $this->db_lab->where($Where);
        $query = $this->db_lab->get();
        return $query;
    }

    function getTable(){
        $this->db_lab->select('*');
        $this->db_lab->from('room');
        $this->db_lab->where('statusflag','Y');
        $this->db_lab->order_by('sq','ASC');
        $query = $this->db_lab->get();
        return $query->result();
    }

    function getToday_Queue(){
        $this->db_lab->select('*');
        $this->db_lab->from('vw_patientdetail_today_ssp');
        $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
        $this->db_lab->where('closed_queue IS NULL',NULL,FALSE);
        $this->db_lab->order_by('vw_patientdetail_today_ssp.mwhen','ASC');
        $query = $this->db_lab->get();
        return $query->result();
        //return ( $query->num_rows() > 0 ? $query->result() : false);
    }

    function getToday_Closed(){
        $this->db_lab->select('*');
        $this->db_lab->from('vw_patientdetail_today_ssp');
        $this->db_lab->where('closed_queue IS NOT NULL',NULL,FALSE);
        $this->db_lab->order_by('mwhen','DESC');
        $query = $this->db_lab->get();
        return $query->result();
        //return ( $query->num_rows() > 0 ? $query->result() : false);
    }

    function getMessage(){
        $query = $this->db_lab->select('*')->from('message')->where('active','Y')->order_by('uid','ASC')->get();
        return $query->result();
    }

    function getNote($data){
        $query = $this->db_lab->select('notedetail')->from('notedetail')->where($data)->order_by('mwhen','DESC')->limit(1)->get();
        return ($query->num_rows() > 0? $query->row() : false );
    }

    function getHold($data){
        $query = $this->db_lab->select(' messageuid, (CASE messageuid WHEN 0 THEN remake ELSE (SELECT description FROM message WHERE message.uid = messagedetail.messageuid ) END) as message')->from('messagedetail')->where($data)->order_by('mwhen','DESC')->limit(1)->get();
        return ($query->num_rows() > 0? $query->row() : false );
    }

    function getRequestInfo($RequestDetail){
        foreach($RequestDetail as $key => $value){
            $this->db_lab->reset_query();
            $this->db_lab->select('*')
                            ->from('vw_patientdetail_today_ssp')
                            ->where('requestno',$value['labRequestNo'])
                            ->limit(1);
            $query = $this->db_lab->get();
            if( $query->num_rows() == 0 ){
                return ['db'=>false,'detail'=>$value];
            }elseif( $query->row()->closed_queue == NULL ){
                return ['db'=>$query->row(),'detail'=>$value];
            }
        }
        return ['db'=>$query->row(),'detail'=>$value];
    }

    function getQueueInfo_Available($data,$returnrow = TRUE){
        //vw_queuewaiting_today_ssp.queue_waiting,
        $this->db_lab->select('vw_patientdetail_today_ssp.*,vw_queuewaiting_today_ssp.queue_waiting_time')->from('vw_patientdetail_today_ssp');
        $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
        $this->db_lab->where($data);
        $this->db_lab->order_by('closed_queue','DESC');
        $this->db_lab->order_by('patientdetail_uid','DESC');
        $this->db_lab->limit(1);
        $query = $this->db_lab->get();
        return ($query->num_rows() > 0? ($returnrow ? $query->row():$query->result()) : false );
    }

    function getQueueInfo($data){
        $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');
        $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
        $this->db_lab->where($data);
        $query = $this->db_lab->get();
        return ($query->num_rows() > 0? $query->row() : false );
    }

    function getQueueWaiting(){
        //,queue_waiting
        $this->db_lab->select('queueno,queue_waiting_time');
        $this->db_lab->from('vw_queuewaiting_today_ssp');
        $query = $this->db_lab->get();
        return ($query->num_rows() > 0? $query->result() : false );
    }

    function getPatientDetail($data = NULL){
        $this->db_lab->select('*')->from('patientdetail');
        if(isset($data->patientuid) && $data->patientuid){
            $this->db_lab->where('uid',$data->patientuid);
        }else if(isset($data->queueno) && $data->queueno){
            $this->db_lab->where('queueno',$data->queueno);
        }else if(isset($data->hn) && $data->hn){
            $this->db_lab->where('hn',$data->hn);
        }
        $patientdetail = $this->db_lab->get();
        return ($patientdetail->num_rows() > 0 ? ($data ? $patientdetail->row() : $patientdetail->result()) : false);
    }

    function CallQueue($data = NULL){
        $check_array = array(
            'patientdetailuid'=> ($data->patientuid ? $data->patientuid : NULL),
            'roomno'=> ($data->tableuid ? $data->tableuid : NULL),
            'cast(createdate as date) = ' => 'cast(now() as date)',
            'cast(cwhen as date) = ' => 'cast(now() as date)',
        );
        $this->db_lab->select('*')->from('callqueue');
        foreach($check_array as $key => $value){
            $this->db_lab->where($key,$value,FALSE);
        }
        $check = $this->db_lab->get();
        $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);
        if ($check->num_rows() > 0) {
            $this->db_lab->reset_query();
            $UpdateData = array(
                'muser' => $cuser,
                'mwhen' => date("Y-m-d H:i:s"),
            );            
            $WhereData = array(
                'patientdetailuid'=> ($data ? $data->patientuid : NULL),
                'roomno'=> ($data ? $data->tableuid : NULL),
                'cast(createdate as date) = ' => 'cast(now() as date)',
                'cast(cwhen as date) = ' => 'cast(now() as date)',
            );
            foreach($WhereData as $key => $value){
                $this->db_lab->where($key,$value,FALSE);
            }
            $this->db_lab->update('callqueue', $UpdateData);
            if($this->db_lab->affected_rows()){
                return $this->InsertProcessControl(2,$data);
            }else{
                return false;
            }
        }else{
            $InsertData = array(
                'patientdetailuid'=> ($data ? $data->patientuid : NULL),
                'roomno'=> ($data ? $data->tableuid : NULL),
                'createdate' => date("Y-m-d H:i:s"),
                'cuser' => $cuser,
                'cwhen' => date("Y-m-d H:i:s"),
                'muser' => $cuser,
                'mwhen' => date("Y-m-d H:i:s"),
            );
            $this->db_lab->insert('callqueue', $InsertData);
            if($this->db_lab->affected_rows()){
                return $this->InsertProcessControl(2,$data);
            }else{
                return false;
            }
        }
        /*
        $check_patient = $this->db_lab->select('*')->from('patientdetail')->where('uid',$data->patientuid)->get();
        if ($check_patient->num_rows() > 0) {
            $this->db_lab->reset_query();
            $check_array = array(
                'patientdetailuid'=> ($data->patientuid ? $data->patientuid : NULL),
                'roomno'=> ($data->tableuid ? $data->tableuid : NULL),
                'cast(createdate as date) = ' => 'cast(now() as date)',
                'cast(cwhen as date) = ' => 'cast(now() as date)',
            );
            $this->db_lab->select('*')->from('callqueue');
            foreach($check_array as $key => $value){
                $this->db_lab->where($key,$value,FALSE);
            }
            $check = $this->db_lab->get();
            $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);
            if ($check->num_rows() > 0) {
                $this->db_lab->reset_query();
                $UpdateData = array(
                    'muser' => $cuser,
                    'mwhen' => date("Y-m-d H:i:s"),
                );            
                $WhereData = array(
                    'patientdetailuid'=> ($data ? $data->patientuid : NULL),
                    'roomno'=> ($data ? $data->tableuid : NULL),
                    'cast(createdate as date) = ' => 'cast(now() as date)',
                    'cast(cwhen as date) = ' => 'cast(now() as date)',
                );
                foreach($WhereData as $key => $value){
                    $this->db_lab->where($key,$value,FALSE);
                }
                $this->db_lab->update('callqueue', $UpdateData);
                if($this->db_lab->affected_rows()){
                    return $this->InsertProcessControl(2,$data);
                }else{
                    return false;
                }
            }else{
                $InsertData = array(
                    'patientdetailuid'=> ($data ? $data->patientuid : NULL),
                    'roomno'=> ($data ? $data->tableuid : NULL),
                    'createdate' => date("Y-m-d H:i:s"),
                    'cuser' => $cuser,
                    'cwhen' => date("Y-m-d H:i:s"),
                    'muser' => $cuser,
                    'mwhen' => date("Y-m-d H:i:s"),
                );
                $this->db_lab->insert('callqueue', $InsertData);
                if($this->db_lab->affected_rows()){
                    return $this->InsertProcessControl(2,$data);
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
        */
    }

    function CloseQueue($data = NULL){
        $check_patient = $this->db_lab->select('*')->from('patientdetail')->where('uid',$data->patientuid)->get();
        if ($check_patient->num_rows() > 0) {
            $this->db_lab->reset_query();
            $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');
            $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
            $check_active = $this->db_lab->where('closed_queue IS NULL',NULL,FALSE)->where('patientdetail_uid',$data->patientuid)->get();
            return ($check_active->num_rows() > 0 ? $this->InsertProcessControl(8,$data) : false);
        }else{
            return false;
        }
    }

    function RevertQueue($data = NULL){
        $check_patient = $this->db_lab->select('*')->from('patientdetail')->where('uid',$data->patientuid)->get();
        if ($check_patient->num_rows() > 0) {
            $queueno = $check_patient->row()->queueno;
            $this->db_lab->reset_query();
            $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');            
            $check_active = $this->db_lab->where('closed_queue IS NULL',NULL,FALSE)->where('queueno',$queueno)->get();
            return ($check_active->num_rows() > 0 ? false : $this->InsertProcessControl(9,$data));
            //$check_active = $this->db_lab->where('closed_queue IS NOT NULL',NULL,FALSE)->where('patientdetail_uid',$data->patientuid)->get();
            //return ($check_active->num_rows() > 0 ? $this->InsertProcessControl(5,$data) : false);
        }else{
            return false;
        }
    }

    function InsertProcessControl($worklistuid,$data = NULL){
        $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);
        $InsertData = array(
            'patientdetailuid'=> ($data ? $data->patientuid : NULL),
            'worklistuid'=> $worklistuid,
            'createdate' => 'NOW()',//date("Y-m-d H:i:s"),
            'cuser' => "'$cuser'",
            'cwhen' => 'NOW()',//date("Y-m-d H:i:s"),
        );
        $this->db_lab->insert('processcontrol', $InsertData,FALSE);
        if($this->db_lab->affected_rows() && $worklistuid == 5){
            $this->db_lab->reset_query();
            $UpdateData = array(
                'muser' => $cuser,
                'mwhen' => 'NOW()',//date("Y-m-d H:i:s"),
            );
            $WhereData = array(
                'uid'=> $data->patientuid,
                'cast(cwhen as date) = ' => 'cast(now() as date)',
            );
            foreach($WhereData as $key => $value){
                $this->db_lab->where($key,$value,FALSE);
            }
            $this->db_lab->set($UpdateData,FALSE);
            $this->db_lab->update('patientdetail');
            return ($this->db_lab->affected_rows()? true : false );
        }else{
            return ($this->db_lab->affected_rows()? true : false );
        }
    }

    function InsertNote($data){
        $check_array = array(
            'patientdetailuid'=> $data->patientuid,
            'cast(cwhen as date) = ' => 'cast(now() as date)',
        );
        $this->db_lab->select('*')->from('notedetail');
        foreach($check_array as $key => $value){
            $this->db_lab->where($key,$value,FALSE);
        }
        $check = $this->db_lab->get();
        $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);
        if ($check->num_rows() > 0) {
            $this->db_lab->reset_query();
            $UpdateData = array(
                'notedetail'=> $data->textarea_note,
                'muser' => $cuser,
                'mwhen' => date("Y-m-d H:i:s"),
            );
            $WhereData = array(
                'patientdetailuid'=> $data->patientuid,
                'cast(cwhen as date) = ' => 'cast(now() as date)',
            );
            foreach($WhereData as $key => $value){
                $this->db_lab->where($key,$value,FALSE);
            }
            $this->db_lab->update('notedetail', $UpdateData);
        }else{
            $InsertData = array(
                'patientdetailuid'=> $data->patientuid,
                'notedetail'=> $data->textarea_note,
                'cuser' => $cuser,
                'cwhen' => date("Y-m-d H:i:s"),
                'muser' => $cuser,
                'mwhen' => date("Y-m-d H:i:s"),
            );
            $this->db_lab->insert('notedetail', $InsertData);
        }
        return ($this->db_lab->affected_rows()? true : false );
    }

    function InsertHold($data){
        $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);
        $InsertData = array(
            'patientdetailuid'=> $data->patientuid,
            'messageuid'=> $data->messageuid,
            'remake'=> isset($data->remake)&&$data->remake?$data->remake:NULL,
            'cuser' => $cuser,
            'cwhen' => date("Y-m-d H:i:s"),
            'muser' => $cuser,
            'mwhen' => date("Y-m-d H:i:s"),
        );
        $this->db_lab->insert('messagedetail', $InsertData);
        return ($this->db_lab->affected_rows()? true : false );
    }

    function reInsertPatientDetail($data){
        $sql = " 
        INSERT INTO patientdetail(hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, cwhen, muser, mwhen, queueno, statusflag, secure)
        SELECT hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, NOW(), muser, NOW(), queueno, statusflag, secure
        FROM patientdetail WHERE hn = '$data->hn' AND cwhen::date = NOW()::date ORDER BY cwhen DESC LIMIT 1
        ";
        $this->db_lab->query($sql);
        $affected_rows = $this->db_lab->affected_rows();
        if($affected_rows){
            $ReturnID = $this->db_lab->insert_id();
            $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');
            $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
            $ReturnResult = $this->db_lab->where('patientdetail_uid', $ReturnID)->get();
            $ReturnResult = $ReturnResult->row();
        }
        return ( $affected_rows ? $ReturnResult : false );
    }
    
    public function RandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function InsertPatientRequest($data){
        $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);
        $check = $this->db_lab->from('vw_patientdetail_today_ssp')->where('closed_queue',NULL)->where('requestno',$data->labrequestno)->get();    
        if ($check->num_rows() == 0) {
            $this->db_lab->reset_query();
            $InsertData = array(
                'hn' => "'$data->hn'",//$data->hn,
                'en' => "'$data->en'",//'-',//$data->vn,
                'queueno' => "'$data->tokenno'",//$data->tokenno,
                'secure' => "'$data->labrequestno'",//$data->secure_code,
                'nation' => "''",//$data->nation,
                'prename' => "'$data->titleth'",//$data->title_th,
                'forename' => "'$data->firstnameth'",//$data->forename_th,
                'surname' => "'$data->lastnameth'",//$data->surname_th,
                'cuser' => "'$cuser'",//$cuser,
                'cwhen' => "NOW()",//date("Y-m-d H:i:s"),
                'muser' => "'$cuser'",//$cuser,
                'mwhen' => "NOW()",//date("Y-m-d H:i:s"),
                'statusflag' => "'Y'",//'Y'
                'locationuid' => "'$data->locationuid'",
                'patienttype' => "$data->patienttype",
                'requestno' => "'$data->labrequestno'",
                'ssp' => "true"
            );
            $this->db_lab->insert('patientdetail', $InsertData,FALSE);
            $affected_rows = $this->db_lab->affected_rows();
            if( $affected_rows ){
                $ReturnID = $this->db_lab->insert_id();
                $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');
                $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
                $ReturnResult = $this->db_lab->where('patientdetail_uid', $ReturnID)->get();
                $ReturnResult = $ReturnResult->row();
            }
            return ( $affected_rows ? $ReturnResult : false );
        }
        return false;
    }

    function reInsertPatientRequest($data){
        $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);        
        $check = $this->db_lab->select('closed_queue')->from('vw_patientdetail_today_ssp')->where('closed_queue',NULL)->where(['queueno'=>$data->queueno,'hn'=>$data->hn,'en'=>$data->vn])->get();
        if ($check->num_rows() == 0) {
            $this->db_lab->reset_query();
            $check = $this->db_lab->from('vw_patientdetail_today_ssp')->where('closed_queue','Y')->where(['queueno'=>$data->queueno,'hn'=>$data->hn,'en'=>$data->vn])->get()->row();
            $this->db_lab->reset_query();
            $InsertData = array(
                'hn' => "'$check->hn'",//$data->hn,
                'en' => "'$check->en'",//'-',//$data->vn,
                'queueno' => "'$check->queueno'",//$data->tokenno,
                'secure' => "'$check->secure'",//$data->secure_code,
                'nation' => "'$check->nation'",//$data->nation,
                'prename' => "'$check->prename'",//$data->title_th,
                'forename' => "'$check->forename'",//$data->forename_th,
                'surname' => "'$check->surname'",//$data->surname_th,
                'cuser' => "'$cuser'",//$cuser,
                'cwhen' => "NOW()",//date("Y-m-d H:i:s"),
                'muser' => "'$cuser'",//$cuser,
                'mwhen' => "NOW()",//date("Y-m-d H:i:s"),
                'statusflag' => "'Y'",//'Y'
                'locationuid' => "'$check->locationuid'",
                'patienttype' => "$check->patienttype",
                'requestno' => "'$check->requestno'",
                'ssp' => "true"
            );
            $this->db_lab->insert('patientdetail', $InsertData,FALSE);
            $affected_rows = $this->db_lab->affected_rows();
            if( $affected_rows ){
                $ReturnID = $this->db_lab->insert_id();
                $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');
                $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
                $ReturnResult = $this->db_lab->where('patientdetail_uid', $ReturnID)->get();
                $ReturnResult = $ReturnResult->row();
            }
        }
        return ( $affected_rows ? $ReturnResult : false );

    }

    function InsertPatientDetail($data){
        $location = isset($data->location)?$data->location:NULL;
        $patienttype = isset($data->patienttype)?$data->patienttype:0;
        $check_array = array(
            'hn'=> "'".$data->hn."'",
            'en'=> "'-'",//$data->vn,            
            'cast(cwhen as date) = ' => 'cast(now() as date)',
            //'queueno'=> $data->tokenno,
        );
        $this->db_lab->select('*')->from('patientdetail');
        foreach($check_array as $key => $value){
            $this->db_lab->where($key,$value,FALSE);
        }
        $check = $this->db_lab->get();
        $cuser = (!$this->session->userdata('userlogin')?NULL:$this->session->userdata('userlogin')['uid']);
        $cuser = is_int($cuser)?$cuser:0;
        if ($check->num_rows() > 0) {
            $chkrow = $check->row();
            $this->db_lab->reset_query();
            $vw_check = $this->db_lab->from('vw_patientdetail_today_ssp')->where('closed_queue',NULL)->where('hn',$chkrow->hn)->get();
            if($vw_check->num_rows() > 0){
                if(isset($data->uid)){
                    $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');
                    $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
                    $ReturnResult = $this->db_lab->where('patientdetail_uid', $data->uid)->get();
                    $ReturnResult = $ReturnResult->row();      
                    return $ReturnResult;      
                }
                return false;
            }else{
                $this->db_lab->reset_query();
                $this->db_lab->select("EXISTS ( SELECT 1 FROM queuenumber WHERE SUBSTRING(lasttokenno,'\D+') = SUBSTRING(patientdetail.queueno,'\D+') AND cwhen::date = NOW()::date ) as genqueue")
                                ->from('patientdetail')
                                ->where('hn',$data->hn)
                                ->where('cwhen::date = ','NOW()::date',FALSE)
                                ->order_by('cwhen','DESC')
                                ->limit(1);
                if($this->db_lab->get()->row()->genqueue){
                    $queueno = $this->generateQueue('L');
                    $random_secure_code =  $this->RandomString();
                    $sql = "
                    INSERT INTO patientdetail(hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, cwhen, muser, mwhen, queueno, statusflag, secure, patienttype,ssp)
                        SELECT hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, NOW(), muser, NOW(), '$queueno' as queueno, 'Y', '$random_secure_code' as secure, patienttype,true as ssp
                        FROM patientdetail WHERE hn = '$data->hn' AND cwhen::date = NOW()::date ORDER BY cwhen DESC LIMIT 1
                    ";
                }else{
                    $sql = " 
                    INSERT INTO patientdetail(hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, cwhen, muser, mwhen, queueno, statusflag, secure, patienttype,ssp)
                        SELECT hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, NOW(), muser, NOW(), queueno, 'Y', secure, patienttype,true as ssp
                        FROM patientdetail WHERE hn = '$data->hn' AND cwhen::date = NOW()::date ORDER BY cwhen DESC LIMIT 1
                    ";
                }
                $this->db_lab->query($sql);
            }
        }else{
            if(isset($data->uid)){
                $data->queueno = $this->generateQueue('L');
                $random_secure_code =  $this->RandomString();
                $sql = " 
                INSERT INTO patientdetail(hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, cwhen, muser, mwhen, queueno, statusflag, secure, patienttype,ssp)
                    SELECT hn, en, idcard, prename, forename, surname, sex, locationuid, nation, queuelocationuid, cuser, NOW(), muser, NOW(), '$data->queueno', 'Y', '$random_secure_code' as secure, patienttype,true as ssp
                    FROM patientdetail WHERE uid = '$data->uid' ORDER BY cwhen DESC LIMIT 1
                ";
                $this->db_lab->query($sql);
            }else{                
                if(!isset($data->secure_code)){
                    $this->load->model('MDL_THG');
                    $SearchPatient = array(
                        'hn'=>$data->hn,
                        'vn'=>$data->vn,
                        'tokenno'=>$data->tokenno,
                    );
                    $data = $this->MDL_THG->getPatientRow($SearchPatient);
                }
                $this->db_lab->reset_query();
                $InsertData = array(
                    'hn' => "'$data->hn'",//$data->hn,
                    'en' => "'-'",//'-',//$data->vn,
                    'queueno' => "'$data->tokenno'",//$data->tokenno,
                    'secure' => "'$data->secure_code'",//$data->secure_code,
                    'nation' => "'$data->nation'",//$data->nation,
                    'prename' => "'$data->title_th'",//$data->title_th,
                    'forename' => "'$data->forename_th'",//$data->forename_th,
                    'surname' => "'$data->surname_th'",//$data->surname_th,
                    'cuser' => "'$cuser'",//$cuser,
                    'cwhen' => "NOW()",//date("Y-m-d H:i:s"),
                    'muser' => "'$cuser'",//$cuser,
                    'mwhen' => "NOW()",//date("Y-m-d H:i:s"),
                    'statusflag' => "'Y'",//'Y'
                    'location' => "'$location'",
                    'patienttype' => "$patienttype",
                    'ssp' => "true"
                );
                $this->db_lab->insert('patientdetail', $InsertData,FALSE);//$this->db_lab->insert('patientdetail', $InsertData);
            }
        }
        $affected_rows = $this->db_lab->affected_rows();
        if( $affected_rows ){
            $ReturnID = $this->db_lab->insert_id();
            $this->db_lab->select('*')->from('vw_patientdetail_today_ssp');
            $this->db_lab->join('vw_queuewaiting_today_ssp','patientdetail_uid','left');
            $ReturnResult = $this->db_lab->where('patientdetail_uid', $ReturnID)->get();
            $ReturnResult = $ReturnResult->row();
        }
        return ( $affected_rows ? $ReturnResult : false );
    }

    function toggleTable($data){
        $this->db_lab->reset_query();
        $UpdateData = array(
            'active'=> $data->active,
        );
        $WhereData = array(
            'uid'=> $data->tableuid,
        );
        foreach($WhereData as $key => $value){
            $this->db_lab->where($key,$value,FALSE);
        }
        $this->db_lab->update('room', $UpdateData);
        $affected_rows = $this->db_lab->affected_rows();
        return ( $affected_rows ? true : false );
    }

    function generateQueue($Code = 'L'){
        $this->db_lab->reset_query();
        $update_sql = "
        UPDATE queuenumber 
        SET lasttokenno = (SUBSTRING(lasttokenno,'\D+') || lpad( ( (SUBSTRING(lasttokenno,'\d+')::int) + 1 )::text, 4, '0')), mwhen = NOW() 
        WHERE SUBSTRING(lasttokenno,'\D+') = '$Code' AND cwhen::date = NOW()::date 
        RETURNING lasttokenno as lasttokenno
        ";
        $result = $this->db_lab->query($update_sql)->row_array();
        if( $this->db_lab->affected_rows() > 0 ){
            return $result['lasttokenno'];
        }else{
            $this->db_lab->reset_query();
            $insert_sql = "
            INSERT INTO queuenumber (lasttokenno, cwhen, mwhen)
                SELECT '$Code'||lpad( 1::text, 4, '0'), NOW(), NOW()
                WHERE NOT EXISTS (SELECT 1 FROM queuenumber WHERE SUBSTRING(lasttokenno,'\D+') = 'L' AND cwhen::date = NOW()::date) 
            RETURNING lasttokenno  as lasttokenno;
            ";
            $result = $this->db_lab->query($insert_sql);
            return "${Code}0001";
        }
    }

}