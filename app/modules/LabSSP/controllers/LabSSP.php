<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LabSSP extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('assets');
		$this->load->helper('json');
		$this->load->helper('recursiveconvert');
		$this->load->helper('calculate');
		$this->load->model('MDL_LabSSP');
	}

	public function index(){
		if( $this->session->userdata('recent_management') ){
			$Recent = $this->session->userdata('recent_management');
			redirect('LabSSP/'.$Recent);
		}else{
			redirect('LabSSP/Lab_Main');
		}
	}
	//Logout
	public function Logout(){
		$this->session->unset_userdata('userlogin');
		redirect('LabSSP');
		return true;
	}

	private function renderView($Data){
		$this->load->module('Template_Module');
		$Data['Module'] = 'LabSSP';
		$this->template_module->Template('Lab_tem', $Data);
	}
	// JSON View
	public function MainManagement(){
		$this->session->set_userdata('recent_management', 'Lab_Main');
		$user_floor = $this->session->userdata('userlogin')['locationuid'];
		header( "Content-Type: application/json" );
		$data = array(
            'Data' => array(
				'userfloor' => $user_floor,
				'table' => $this->MDL_LabSSP->getTable(),
				'queuedata' => $this->MDL_LabSSP->getToday_Queue(),
				'holdmessage' => $this->MDL_LabSSP->getMessage(),
			),
		);
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] ? "LabSSP/Lab_Queue_Location_vw" : "LabSSP/Lab_Queue_vw"; 
		$output = array(
			'html'=>$this->load->view($view,$data,TRUE),
		);
		echo json_encode($output);
	}
	public function MainTable(){
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] ? "Lab/json_views/Lab_Queue_Location_TR" : "Lab/json_views/Lab_Queue_TR"; 
		$viewsData = array(
			'Data' => array(
				'queuedata' => $this->MDL_LabSSP->getToday_Queue(),
			),
		);
		header( "Content-Type: application/json" );
		$message = array(
			'method' => 'MainTable',
			'result' => 'Success',
			'target' => '#Lab_Queue_TBody',
			'html' => $this->load->view($view,$viewsData,TRUE),
		);
		echo json_response(200, $message);
	}
	public function ClosedManagement(){
		$this->session->set_userdata('recent_management', 'Lab_Closed');
		header( "Content-Type: application/json" );
		$data = array(
            'Data' => array(
				'queuedata' => $this->MDL_LabSSP->getToday_Closed(),
			),
		);
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] ? "Lab/Lab_Closed_Location_vw" : "Lab/Lab_Closed_vw"; 
		$output = array(
			'html'=>$this->load->view($view,$data,TRUE),
		);
		echo json_encode($output);
	}
	public function ClosedTable(){
		$view = isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser'] ? "Lab/json_views/Lab_Closed_Location_TR" : "Lab/json_views/Lab_Closed_TR"; 
		$viewsData = array(
			'Data' => array(
				'queuedata' => $this->MDL_LabSSP->getToday_Closed(),
			),
		);
		header( "Content-Type: application/json" );
		$message = array(
			'method' => 'ClosedTable',
			'result' => 'Success',
			'target' => '#Lab_Closed_TBody',
			'html' => $this->load->view($view,$viewsData,TRUE),
		);
		echo json_response(200, $message);
	}

	// Main
	public function Lab_Main(){
		if(!$this->session->userdata('userlogin')){
			redirect(base_url(),'refresh');
		}
		$Template = array(
			'Site_Title' => 'Dashqueue',
			'Script' => array(
				'Script' => 'Lab_script',
			),
            'Data' => array(
				'table' => $this->MDL_LabSSP->getTable(),
				'queuepage' => base_url('LabSSP/MainManagement'),
			),
		);
		$this->renderView($Template);
	}
	public function Lab_Closed(){
		if(!$this->session->userdata('userlogin')){
			redirect(base_url(),'refresh');
		}
		$Template = array(
			'Site_Title' => 'Dashqueue',
			'Script' => array(
				'Script' => 'Lab_script',
			),
            'Data' => array(
				'queuepage' => base_url('LabSSP/ClosedManagement'),
			),
		);
		$this->renderView($Template);
	}

	public function Queue_Call($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_LabSSP->CallQueue($InsertData);
			if($InsertResult){
				$cURLCall = $this->process_call($InsertData);
				$cURLTrigger = $this->process_trigger( (isset($InsertData->tableuid)?$InsertData->tableuid:0) );
				if($JSONResponse){
					$message = array(
						'method' => 'Call Queue',
						'result' => 'Success',
						'active' => 'button.action_call[data-patientuid="'.$InsertData->patientuid.'"]',
						'trigger' => $cURLTrigger,
						'callcURL' => $cURLCall, 
					);
					echo json_response(200, $message);
				}else{
					return $InsertResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Call Queue',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_LabSSP->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'tableuid' => (isset($InsertData->tableuid) && $InsertData->tableuid ? $InsertData->tableuid : NULL ),
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Call($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Call Queue',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}

		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Call Queue',
					'result' => 'Failed',
					'text' => 'No Patientuid/Queueno',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function Queue_Note($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_LabSSP->InsertNote($InsertData);
			if($InsertResult){
				if($JSONResponse){
					$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
					$message = array(
						'method' => 'Insert Note',
						'result' => 'Success',
						'text' => 'Insert Success',
						'active' => 'button.action_note[data-patientuid="'.$InsertData->patientuid.'"]',
					);
					echo json_response(200, $message);
				}else{
					return ($InsertResult != false ? $InsertResult : false);
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Note',
						'result' => 'Failed',
						'text' => 'Insert Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_LabSSP->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'textarea_note' => $InsertData->textarea_note,
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Note($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Note',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}	
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Insert Note',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function Queue_Hold($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_LabSSP->InsertHold($InsertData);
			if($InsertResult){
				if($JSONResponse){
					$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
					$message = array(
						'method' => 'Insert Hold',
						'result' => 'Success',
						'text' => 'Insert Success',
						'active' => 'button.action_hold[data-patientuid="'.$InsertData->patientuid.'"]',
					);
					echo json_response(200, $message);
				}else{
					return ($InsertResult != false ? $InsertResult : false);
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Hold',
						'result' => 'Failed',
						'text' => 'Insert Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_LabSSP->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'messageuid' => isset($InsertData->messageuid) && $InsertData->messageuid?$InsertData->messageuid:(isset($InsertData->remake) && $InsertData->remake?0:NULL),
					'remake' => isset($InsertData->remake) && $InsertData->remake?0:NULL,
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Hold($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Insert Hold',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}	
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Insert Hold',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function Queue_Close($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_LabSSP->CloseQueue($InsertData);
			if($InsertResult){
				$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
				if($JSONResponse){
					$message = array(
						'method' => 'Close Queue',
						'result' => 'Success',
					);
					if(isset($InsertData->queueno) && $InsertData->queueno) $message['remove'] = '#RowQueue_'.$InsertData->queueno;
					echo json_response(200, $message);
				}else{
					return $InsertResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Close Queue',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_LabSSP->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Close($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Close Queue',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}

		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Close Queue',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}	
	public function Queue_Revert($Data = NULL,$JSONResponse = TRUE){
		$InsertData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($InsertData->patientuid) && $InsertData->patientuid){
			$InsertResult = $this->MDL_LabSSP->RevertQueue($InsertData);
			if($InsertResult){
				$this->process_trigger( ($this->session->userdata('userlogin')['locationuid']?$this->session->userdata('userlogin')['locationuid']:0) );
				if($JSONResponse){
					$message = array(
						'method' => 'Revert Queue',
						'result' => 'Success',
					);
					if(isset($InsertData->queueno) && $InsertData->queueno) $message['remove'] = '#RowClosedQueue_'.$InsertData->queueno;
					echo json_response(200, $message);
				}else{
					return $InsertResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Revert Queue',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else if(isset($InsertData->queueno) && $InsertData->queueno){
			$Search = to_stdClass(array(
				'queueno' => $InsertData->queueno,
			));
			$PatientDetail = $this->MDL_LabSSP->getPatientDetail($Search);
			if($PatientDetail){
				$NewData = array(
					'patientuid' => $PatientDetail->uid,
					'queueno' => $PatientDetail->queueno,
				);
				$this->Queue_Revert($NewData,$JSONResponse);
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Revert Queue',
						'result' => 'Failed',
						'text' => 'Not Found Patient Info From Queueno',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}

		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Revert Queue',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	public function toggleTable($Data = NULL,$JSONResponse = TRUE){
		$UpdateData = to_stdClass(($Data ? $Data : ($this->input->post() !== NULL ? $this->input->post() : array())));
		if(isset($UpdateData->tableuid) && isset($UpdateData->active)){
			$UpdateResult = $this->MDL_LabSSP->toggleTable($UpdateData);
			if($UpdateResult){
				$cURLTrigger = $this->process_table();
				if($JSONResponse){
					$message = array(
						'method' => 'Toggle Table',
						'result' => 'Success',
						'TriggercURL' => $cURLTrigger,
					);
					echo json_response(200, $message);
				}else{
					return $UpdateResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Toggle Table',
						'result' => 'Failed',
					);
					echo json_response(200, $message);				
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Toggle Table',
					'result' => 'Failed',
					'text' => 'Invalid Input',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	// cURL Process
	private function get_cURL($URL){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$URL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	private function post_cURL($URL,$DATA = ""){
		//$DATA = "queuestring=$text&counter=$call_counter&queue=$call_queue_num&type=$string[0]&locationid=$call_location";
		$POST_FIELD = "";
		foreach($DATA as $Key => $Value){
			$POST_FIELD .= ( $POST_FIELD != "" ? '&':'') . $Key . '=' . $Value;
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$POST_FIELD);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	private function process_trigger($Location = 0){		
		if(isset($this->session->userdata('userlogin')['locationuser']) && $this->session->userdata('userlogin')['locationuser']){
			$location = $this->session->userdata('userlogin')['locationuser'];
			$ch1 = curl_init();
			curl_setopt($ch1, CURLOPT_URL,APITRIGGER."/trigger_display_lab?location_id=$location");
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch1);
			curl_close($ch1);
			echo APITRIGGER."/trigger_display_lab?location_id=$location";
		}
		$TriggerURL = APITRIGGER . '/refresh_display_lab?location_id=' . $Location;
		echo $TriggerURL;
		return $this->get_cURL($TriggerURL);
	}
	private function process_scan(){
		
	}
	private function process_call($Data){
		$TriggerURL = APITRIGGER . '/call_lab';
		$Language = ( !isset($Data->nation) || $Data->nation == 'TH' || $Data->nation == 'ไทย' ? 'TH':'EN' );
		switch ($Language) {
			case 'TH':
				$Word = $Language.'|เชิญหมายเลข|'.implode(str_split($Data->queueno,1),'|') .'|ที่ช่องบริการ|'.$Data->tableuid.'|ค่ะ';
				break;		
			case 'EN':
				$Word = $Language.'|number|'.implode(str_split($Data->queueno,1),'|') .'|desknumber|'.$Data->tableuid;
				break;			
			default:
				$Word = $Language.'|เชิญหมายเลข|'.implode(str_split($Data->queueno,1),'|') .'|ที่ช่องบริการ|'.$Data->tableuid.'|ค่ะ';
				break;
		}
		$POST = array(
			"location_id" => $this->session->userdata('userlogin')['locationuid'],
			"queue" => $Data->queueno,
			"counter" => $Data->tableuid,
			"soundstring" => $Word,
		);
		return $this->post_cURL($TriggerURL,$POST);
	}
	private function process_note(){
		
	}
	private function process_close(){
		
	}
	private function process_revert(){
		
	}
	private function process_table(){
		$TriggerURL = APITRIGGER . '/lab_update_status_counter';
		return $this->get_cURL($TriggerURL);		
	}

	// Get PatientStatus Info
	public function PatientStatus_HN($HN = NULL,$JSONResponse = TRUE){
		if($HN){
			$Search = array(
				'vw_patientdetail_today.hn' => $HN,
			);

			$QueueInfo = $this->MDL_LabSSP->getQueueInfo_Available($Search);
			if($QueueInfo){
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'hn' => $HN,
						'current_queueno' => $QueueInfo->queueno,
						'result' => 'Success',
						'data' => $QueueInfo,
					);
					echo json_response(200, $message);
				}else{
					return $QueueInfo;
				}			
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'hn' => $HN,
						'result' => 'Failed',
						'text' => 'Not Found Queue Info',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'getQueueInfo',
					'result' => 'Failed',
					'text' => 'Invalid Queueno Input',
				);
				echo json_response(400, $message);
			}else{
				return false;
			}
		}		
	}

	// Get Queue Info
	public function QueueInfo_Queueno($Queueno = NULL,$JSONResponse = TRUE){
		if($Queueno){
			$Search = array(
				'vw_patientdetail_today.queueno' => $Queueno,
			);

			$QueueInfo = $this->MDL_LabSSP->getQueueInfo($Search);
			if($QueueInfo){
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'queueno' => $Queueno,
						'result' => 'Success',
						'data' => $QueueInfo,
					);
					echo json_response(200, $message);
				}else{
					return $QueueInfo;
				}			
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'getQueueInfo',
						'queueno' => $Queueno,
						'result' => 'Failed',
						'text' => 'Not Found Queue Info',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'getQueueInfo',
					'result' => 'Failed',
					'text' => 'Invalid Queueno Input',
				);
				echo json_response(400, $message);
			}else{
				return false;
			}
		}		
	}
	// Check Queue Closed
	public function QueueClosed($Queueno,$JSONResponse = TRUE){
		$Search = array(
			'vw_patientdetail_today.queueno' => $Queueno,
		);

		$QueueInfo = $this->MDL_LabSSP->getQueueInfo($Search);
		if($QueueInfo){
			if($JSONResponse){
				$message = array(
					'method' => 'getQueueInfo',
					'queueno' => $Queueno,
					'result' => 'Success',
					'closed' => ($QueueInfo->closed_queue != NULL ? true : false),
				);
				echo json_response(200, $message);
			}else{
				return ($QueueInfo->closed_queue != NULL ? true : false);
			}			
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Check Queue Closed',
					'queueno' => $Queueno,
					'result' => 'Failed',
					'text' => 'Not Found Queue Info',
				);
				echo json_response(200, $message);
			}else{
				return false;
			}
		}
	}
	// Get Queue Waiting
	public function getQueue_Waiting($JSONReponse = TRUE){
		$Data = $this->MDL_LabSSP->getQueueWaiting();
		if($JSONReponse){
			if($Data){
				$message = array(
					'method' => 'getQueue_Waiting',
					'result' => 'Success',
					'data' => $Data,
				);
				echo json_response(200, $message);
			}else{
				$message = array(
					'method' => 'getQueue_Waiting',
					'result' => 'Failed',
					'text' => 'Not Found Data',
				);
				echo json_response(200, $message);
			}
		}else{
			return $Data;
		}
	}
	// Get Queue Note
	public function getQueue_Note($PUID = NULL,$JSONResponse = TRUE){
		$Search = array('patientdetailuid'=>$PUID);
		
		if($PUID != NULL){
			$SearchResult = $this->MDL_LabSSP->getNote($Search);
			if($SearchResult){
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Note',
						'result' => 'Success',
						'data' => $SearchResult,
					);
					echo json_response(200, $message);
				}else{
					return $SearchResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Note',
						'result' => 'Failed',
						'text' => 'Not Found Note',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Get Queue Note',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	// Get Queue Hold
	public function getQueue_Hold($PUID = NULL,$JSONResponse = TRUE){
		$Search = array('patientdetailuid'=>$PUID);
		
		if($PUID != NULL){
			$SearchResult = $this->MDL_LabSSP->getHold($Search);
			if($SearchResult){
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Hold',
						'result' => 'Success',
						'data' => $SearchResult,
					);
					echo json_response(200, $message);
				}else{
					return $SearchResult;
				}
			}else{
				if($JSONResponse){
					$message = array(
						'method' => 'Get Queue Hold',
						'result' => 'Failed',
						'text' => 'Not Found Hold',
					);
					echo json_response(200, $message);
				}else{
					return false;
				}
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Get Queue Hold',
					'result' => 'Failed',
					'text' => 'No Patientuid',
				);
				echo json_response(400, $message);				
			}else{
				return false;
			}
		}
	}
	// Get Table
	public function getTable($JSONResponse = TRUE){
		$GetResult = $this->MDL_LabSSP->getTable();
		if($GetResult){
			if($JSONResponse){
				$message = array(
					'method' => 'Get Table',
					'result' => 'Success',
					'data' => $GetResult,
				);
				echo json_response(200, $message);
			}else{
				return $GetResult;
			}
		}else{
			if($JSONResponse){
				$message = array(
					'method' => 'Get Table',
					'result' => 'Failed',
				);
				echo json_response(200, $message);				
			}else{
				return false;
			}
		}
	}

	public function TestModel($Table){
		$this->load->model('MDL_LabSSP');
		$query = $this->MDL_LabSSP->DBSelect($Table);
		echo '<pre>'.var_export($query->result(),TRUE).'</pre>';
	}

	public function Test(){
		return "HI";
	}
}
