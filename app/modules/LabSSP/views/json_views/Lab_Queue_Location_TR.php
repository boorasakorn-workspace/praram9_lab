<?php 
    if( isset($Data['queuedata']) && count($Data['queuedata']) > 0 ){
            foreach($Data['queuedata'] as $queue_key => $queue_value):
?>
    <tr id="RowQueue_<?=$queue_value->queueno;?>">
        <td><?=$queue_key+1;?></td>
        <td>
            <button class="button block btn_action action_call <?=($queue_value->callqueuecwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>" data-nation="<?=$queue_value->nation;?>"><i class="fas fa-volume-up"></i></button>
        </td>
        <td><?=$queue_value->queueno;?></td>
        <td><?=$queue_value->prename . ' ' . $queue_value->forename . ' ' . $queue_value->surname;?></td>
        <td><?=$queue_value->hn;?></td>
        <?php /*
            <td><?=$queue_value->en;?></td>
            <td> </td>
            <td><?=preg_replace('!\d+!', '', $queue_value->queueno);?></td>
            <td><?=$queue_value->sex;?></td>
            <td> </td>
        */ ?>
        <td td_waitingqueue="<?=$queue_value->queueno;?>" <?=$queue_value->queue_waiting_time>14?'style="color: #FF0000"':'';?>><!--<?=duration($queue_value->mwhen);?>--><?=$queue_value->queue_waiting_time;?></td>
        <!-- <td>
            <button class="button block btn_action action_hold <?=($queue_value->messagedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fas fa-ban"></i></button>
        </td> -->
        <td>
            <button class="button block btn_action action_note <?=($queue_value->notedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fa fa-edit"></i></button>
        </td>
        <td>
            <button class="button block btn_action action_close <?=($queue_value->closed_queue != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><span aria-hidden="true">×</span></button>
        </td>
    </tr>
<?php 
        endforeach;
    }
?>