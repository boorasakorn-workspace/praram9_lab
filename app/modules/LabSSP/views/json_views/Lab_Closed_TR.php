<?php 
    if( isset($Data['queuedata']) && count($Data['queuedata']) > 0 ){
        foreach($Data['queuedata'] as $queue_key => $queue_value):
?>
    <tr id="RowClosedQueue_<?=$queue_value->queueno;?>">
        <td><?=$queue_key+1;?></td>
        <td><?=$queue_value->queueno;?></td>
        <td><?=$queue_value->prename . ' ' . $queue_value->forename . ' ' . $queue_value->surname;?></td>
        <td><?=$queue_value->hn;?></td>
        <!-- <td><?=$queue_value->en;?></td> -->
        <?php /*
            <td> </td>
            <td><?=preg_replace('!\d+!', '', $queue_value->queueno);?></td>
            <td><?=$queue_value->sex;?></td>
            <td> </td>
            <td><?=$queue_value->xraydischargedate;?></td>
        */ ?>
        <td>                        
            <button class="button block btn_action action_note <?=($queue_value->notedetailcwhen != NULL?'active':'');?>" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>"><i class="fa fa-edit"></i></button>
        </td>
        <td>
            <button class="button block btn_action action_revert" data-patientuid="<?=$queue_value->patientdetail_uid;?>" data-queueno="<?=$queue_value->queueno;?>">Revert</button>
        </td>
    </tr>
<?php 
        endforeach;
    }
?>