<!DOCTYPE html>
<html>

<head>
	<title><?= (isset($Site_Title) ? $Site_Title : (defined(SITE_TITLE) ? SITE_TITLE : "DashQueue")); ?></title>
	<link rel="shortcut icon" href='<?= base_url() ?>static/img/q-logo-Recovered.png'>
	<link rel="shortcut icon" type="image/x-icon" href="<?= base_url('static/images/logo_bio.ico'); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?= base_url('static/images/logo_bio.ico'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="content-type" content="text/javascript; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
	$template_css = array(
		'bootstrap-4.4.1' => 'vendor/bootstrap-4.4.1/css/bootstrap.min.css',
		'fontawesome' => 'vendor/fontawesome/css/all.min.css',
		'jqueryDataTable' => 'vendor/jquerydataTable/datatables.min.css',
		'sweetalert' => 'vendor/sweetalert/sweetalert.css',
		'responsivescale' => 'css/ResponsiveScale.css',
		'font' => 'font/pr9/pr9.css'
		// 'main' => 'css/management_style.css',
	);
	echo assets_css($template_css);
	?>
	<?= (isset($css) ? assets_css($css) : ''); ?>


	<style>
		body {
			font-family: 'dbfongnam_reg';
		}
		.main_h{
			font-family: 'dbozone_med';
		}
		th,h1,h2,h3,h4,h5,h6{
			font-family: 'dbozone_med';
		}
		td{
			font-size:3rem;
		}
		label{
			font-family: 'dbozone_reg';
		}
		input::placeholder{
			font-family: 'dbozone_light';
		}
		.page_row button{
			font-family: 'dbozone_reg';
		}
	</style>
	<style>
		body {
			background: rgb(51, 122, 183);
		}

		.center {
			display: flex;
			justify-content: center;
			align-content: center;
			text-align: center;
		}

		td button {
			outline: none;
		}

		thead tr {
			background-attachment: fixed;
			background: #008387;
		}

		.table td,
		.table th {
			padding: .8rem;
		}

		table {
			font-size: 1.2vw;
		}

		div.dataTables_scrollBody table tbody tr:first-child th,
		div.dataTables_scrollBody table tbody tr:first-child td {
			vertical-align: middle !important;
		}

		tr {
			font-weight: 400;
			display: table-row;
			vertical-align: middle;
			border-color: inherit;
		}

		tbody tr {
			height: 3rem;
		}

		tr th {
			color: #ffffff;
			font-weight: 500;
		}

		tbody tr:hover td {
			background-color: #65C294;
		}

		/* CSS4 */
		/*
		tbody tr:hover td:has(button.btn_action){
			background-color: initial;
		}
		*/

		.tb-queue tbody tr:hover td:last-child,
		.tb-queue tbody tr:hover td:nth-last-child(2),
		.tb-queue tbody tr:hover td:nth-child(2) {
			background-color: initial;
		}

		.tb-closed tbody tr:hover td:last-child {
			background-color: initial;
		}

		.ui-datatable.borderless thead th,
		.ui-datatable.borderless tbody,
		.ui-datatable.borderless tbody tr,
		.ui-datatable.borderless tbody td {
			border-style: none;
		}

		.header {
			background: #337ab7;
		}

		button,
		textarea,
		input[type="text"],
		input[type="search"],
		input[type="password"] {
			font-size: 1.75rem !important;
		}

		.tb-queue,
		.tb-closed,
		.btn_action,
		.btn-large {
			font-size: 2.25rem !important;
		}

		.modal-title {
			font-size: 2.5rem;
		}

		.btn_action {
			padding: 0.75rem 0.5rem;
			line-height: 1.5;
			font-size: 1.25rem;
			border-radius: 0.5rem;
			width: 50px;
		}

		.btn_action.action_note,
		.btn_action.action_hold,
		.btn_action.action_call,
		.btn_action.action_close,
		.btn_action.action_revert {
			color: #000000;
			background-color: #C0C0C0;
		}

		.btn_action.action_note.active,
		.btn_action.action_note:hover {
			background-color: #337ab7;
		}

		.btn_action.action_hold.active,
		.btn_action.action_hold:hover {
			background-color: #F2D387;
		}

		.btn_action.action_call.active,
		.btn_action.action_call:hover {
			background-color: #65C294;
		}

		.btn_action.action_close.active,
		.btn_action.action_close:hover {
			background-color: #006286;
		}

		.btn_action.action_revert.active,
		.btn_action.action_revert:hover {
			background-color: #FFCD5B;
		}

		.icon_row img {
			margin: 0 auto;
			width: 10rem;
			height: 10rem;
		}

		.btn_row {
			margin: 0 auto;
			min-height: 80px;
		}

		.main-btn_row button {
			margin: 0 auto;
			padding: 10px 30px;
		}

		.norad-btn_row button {
			border-radius: 0;
			margin: 0 auto;
			padding: 10px 30px;
		}

		.btn-main {
			margin: 0 auto;
			padding: 10px 30px;
		}

		.btn-norad {
			border-radius: 0;
			margin: 0 auto;
			padding: 10px 30px;
		}

		.btn-flat {
			margin: 0 auto;
			padding: 5px 30px;
		}

		.btn-cyan {
			color: #fff;
			background-color: #78bfaf;
			border-color: #78bfaff5;
		}

		.button {
			color: #ffffff;
			white-space: nowrap;
			display: inline-block;
			font-weight: 400;
			text-align: center;
			vertical-align: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			border: 1px solid transparent;
			padding: .25rem .5rem;
			font-size: 1.25rem;
			text-shadow: 0px 0px 20px #CECECE;
			line-height: 1.5;
			border-radius: .2rem;
			transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
		}

		.button:hover {
			color: #000000 !important;
		}

		.management-container {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			/*padding:0rem 2rem 0rem 2rem;*/
			padding: 0 0 0 0rem;
		}

		.management-container>.header {
			display: flex;
			align-items: center;
			color: #ffffff;
			background-color: #008387;
		}

		.management-container>.content {
			width: 100%;
			background-color: #fdfdfd;
			border-right: 3px solid #a5a9ad;
			border-left: 3px solid #a5a9ad;
		}

		.page_row {
			position: relative;
			padding-top: .8rem;
		}

		.page_row::before {
			position: absolute;
			bottom: 0;
			margin-left: -20px;
			width: calc(100vw + 15px);
			content: " ";
			font-size: 0px;
			border-color: #d5d5d5;
			border-style: solid;
			border-width: 0.5px;
		}

		.page_row button {
			position: relative;
			font-size: 1.5rem;
			padding: 10px 26px;
			border-color: #d5d5d5;
			border-style: solid;
			border-width: 1px;
			border-radius: 5px 5px 0 0;
			color: #000000;
			background-color: #e8e8e8;
			min-width: 110px;
		}

		.page_row button:focus {
			outline: 0;
		}

		.page_row button::after {
			content: " ";
			position: absolute;
			bottom: 0;
			left: -1px;
			height: 50%;
			width: calc(100% + 2px);
			border-width: 0.5px;
			border-style: solid;
			border-color: #dddddd;
			border-top: none;
			border-bottom: none;
			z-index: 1001;
		}

		.page_row button.active {
			border-bottom: none;
			background-color: #ffffff;
		}

		.page_row button.active::after {
			content: " ";
			position: absolute;
			bottom: -1px;
			left: 0;
			height: 50%;
			width: 101%;
			border-style: unset;
			z-index: 1002;
		}

		tr th:first-child {
			border-radius: 5px 0px 0px 5px;
			-moz-border-radius: 5px 0px 0px 5px;
			-webkit-border-radius: 5px 0px 0px 5px;
		}

		tr th:last-child {
			border-radius: 0px 5px 5px 0px;
			-moz-border-radius: 0px 5px 5px 0px;
			-webkit-border-radius: 0px 5px 5px 0px;
		}

		tr td:first-child {
			border-radius: 5px 0px 0px 5px;
			-moz-border-radius: 5px 0px 0px 5px;
			-webkit-border-radius: 5px 0px 0px 5px;
		}

		tr td:last-child {
			border-radius: 0px 5px 5px 0px;
			-moz-border-radius: 0px 5px 5px 0px;
			-webkit-border-radius: 0px 5px 5px 0px;
		}

		.none {
			background: transparent;
			border: none;
		}

		.nowrap_container {
			display: flex;
			justify-content: center;
			margin-top: 0rem;
			overflow-x: auto;
			overflow-y: auto;
			height: calc(90% - 10px);
		}

		.block {
			display: block;
			width: 100%;
			padding-top: 0.25em;
			padding-bottom: 0.25em;
		}
		.block-nowidth{
			padding-top: 0.25em;
			padding-bottom: 0.25em;
		}

		.dashboard {
			width: 100%;
			height: 92%;
			padding: 5px;
			border-radius: 30px;
			background-color: #FFFFFF;
		}

		.dataTables_wrapper {
			font-size: 1.5rem;
			width: 100%;
			height: 100%;
		}

		.dataTables_scroll {
			height: 100%;
		}

		div.dataTables_scrollHead table.dataTable {
			width: 100% !important;
			padding-right: 0px !important;
		}

		.dataTables_scrollHeadInner {
			box-sizing: content-box;
			padding-right: 0px !important;
			width: auto !important;
		}

		.dataTables_scroll>.dataTables_scrollBody {
			position: relative;
			overflow: auto;
			width: 100%;
			height: calc(100% - 10%);
		}

		.dataTables_scrollBody,
		.dataTables_scrollHead {
			overflow: hidden;
			position: relative;
			border: 0px;
			width: 100%;
			height: fit-content;
		}

		.dataTables_wrapper .dataTables_paginate {
			float: right !important;
			text-align: right;
			padding-top: 0.25em;
		}

		td.dataTables_empty{
			color: transparent !important;
		}

		.input_icon {
			border-radius: 20px;
		}

		input[type=text].input_icon {
			padding-left: 2.5em;
			padding-right: 3em;
			margin-left: 0rem !important;
			min-height: 40px;
		}

		input[type=text] {
			font-size: 1.5rem;
			text-align: left;
		}

		input[type=text]::-webkit-input-placeholder {
			/* Edge */
			padding-left: 0.5em;
			text-align: left;
		}

		input[type=text]:-ms-input-placeholder {
			/* Internet Explorer 10-11 */
			padding-left: 0.5em;
			text-align: left;
		}

		input[type=text]::placeholder {
			padding-left: 0.5em;
			text-align: left;
		}

		.main_h {
			white-space: nowrap;
			margin: 0;
			font-size: 1.8vw;
			text-shadow: 1px 1px 1px #0d173a;
		}

		.tab_style {
			padding-left: 3px;
			padding-right: 2px;
		}

		table.dataTable {
			width: 100%;
		}

		.lab_on {
			padding: 8px 12px;
			height: fit-content;
			font-size: 1.35rem;
			border: none;
			border-radius: .3rem;
			color: #ffffff;
			background-color: #008387;
		}

		.lab_on.active {
			background-color: #d5d5d5;
		}

		.center_row {
			display: flex;
			align-content: center;
		}

		/* modal */
		.main-btn_row {
			margin: 0 auto;
		}

		.modal-content {
			background-color: #008387 !important;
			border: 2px solid #008387;
			color: #FFFFFF;
		}

		.btnin_call {
			padding: 1rem 2rem !important;
			font-size: 1.6rem;
			border-radius: .6rem !important;
			border: none;
			color: #fff;
			background-color: #78bfaf;
			margin-bottom: 1.2rem;
		}

		.btnin_call.disable {
			background-color: #a1a1a1;
		}

		.btnin_call:hover {
			border: none;
			background-color: #a1a1a1;
		}

		.btnin_call.disable:hover {
			border: none;
			background-color: #78bfaf !important;
		}

		.btnin_call:disabled,
		.btnin_call[disabled]{
			background-color: #a1a1a1;
		}

		.row {
			margin-right: 0;
			margin-left: 0;
		}

		.container-fluid {
			padding-right: 2px;
			padding-left: 2px;
		}

		.modal-body {
			background-color: #ffffff;
			border-radius: 0 0 .3rem 0.3rem;
		}

		.sweet-alert h2 {
			font-size: 3rem !important;
		}
		.sweet-alert p.lead {
			font-size: 2.5rem !important;
		}
	</style>
	<?= single_js('js/viewport.js'); ?>
</head>

<body>
	<div class="management-container" style="height:100%">

		<div id="ManagementOuterTab" class="header row no-gutters" style="padding-left: 1.2vw; height: 6%;display:flex;align-items: center;justify-content: space-between;">
			<div>
				<span class="col-12 main_h">DASHQUEUE</span>
			</div>
			<div>
				<a href="<?=base_url('Lab/Logout');?>">
					<i class="fas fa-sign-out-alt" style="color:#FFFFFF;font-size:16px; padding-right:10px;" id="logout"></i>
				</a>
			</div>
		</div>

		<div id="ManagementDashboard" class="content" style="position: relative;height:94%;">

			<div id="headDashboard" class="container-fluid" style=" height: 8%;">
				<div class="row page_row">
					<a class="tab_style" href="<?= $this->session->userdata('userlogin')['locationuser']?base_url('LabSSP/Lab_Main'):base_url('Lab/Lab_Main'); ?>" data-page="<?= $this->session->userdata('userlogin')['locationuser']?base_url('LabSSP/MainManagement'):base_url('Lab/MainManagement'); ?>" data-subtable="<?= $this->session->userdata('userlogin')['locationuser']?base_url('LabSSP/ClosedTable'):base_url('Lab/MainTable'); ?>">
						<button class="col <?= (strtolower($this->uri->segment(2)) == 'lab_main' ? 'active' : '') ?>">
							รายชื่อผู้ป่วยห้อง Lab
						</button>
					</a>
					<a href="<?= $this->session->userdata('userlogin')['locationuser']?base_url('LabSSP/Lab_Closed'):base_url('Lab/Lab_Closed'); ?>" data-page="<?= $this->session->userdata('userlogin')['locationuser']?base_url('LabSSP/ClosedManagement'):base_url('Lab/ClosedManagement'); ?>" data-subtable="<?= $this->session->userdata('userlogin')['locationuser']?base_url('LabSSP/ClosedTable'):base_url('Lab/ClosedTable'); ?>">
						<button class="col <?= (strtolower($this->uri->segment(2)) == 'lab_closed' ? 'active' : '') ?>">
							ตรวจเสร็จ
						</button>
					</a>
					<div class="col"><a class="btn" button_floor style="border:1px black solid;float: right;"><h1 id="desk_span" ></h1></a></div>
				</div>
			</div>
			<div id="contentDashboard" style=" height: 91%; margin-top: .3rem;">
				<!-- Management Content -->
				<?php
				if (isset($Content) && count($Content) > 0) {
					foreach ($Content as $result) :
						$this->load->view((isset($Module) ? $Module . ($Module != '' ? '/' : '') : '/') . $result);
					endforeach;
				}
				?>
				<!-- /Management Content -->
			</div>

		</div>

	</div>



	<!-- modal note -->
	<div class="modal" tabindex="-1" role="dialog" id="md_note">
		<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
			<div class="modal-content">
				<div class="modal-header center" style="min-height: 3rem; padding: .8rem;">
					<h5 class="modal-title">Note</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body center">
					<div class="container-fluid">
						<form action="<?= base_url('Lab/Queue_Note'); ?>" method="post" enctype="multipart/form-data">
							<input type="hidden" name="queueno">
							<input type="hidden" name="patientuid">
							<div class="row">
								<div class="col" style="padding: .8rem;">
									<textarea class="form-control" rows="5" id="textarea_note" name="textarea_note"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-12 center">
									<button class="btn btn-flat btn-success btn-large" type="submit" id='conf_note'>บันทึก</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php
	$template_js = array(
		'popper' => 'vendor/popper/popper.min.js',
		'jquery3.4.1' => 'js/jquery-3.4.1.min.js',
		'bootstrap-4.4.1' => 'vendor/bootstrap-4.4.1/js/bootstrap.min.js',
		'jqueryDataTable' => 'vendor/jquerydataTable/datatables.min.js',
		'jquery-resizer' => 'vendor/jquery.resize-master/jquery.resize.js',
		'moment' => 'vendor/moment/moment-with-locales.js',
		'sweetalert' => 'vendor/sweetalert/sweetalert.min.js',
		'socketio'=>'vendor/socketio/socket.io.js'
	);
	echo assets_js($template_js);
	?>
	<?= (isset($js) ? assets_js($js) : ''); ?>
	<?= (isset($node_modules) ? assets_node($node_modules) : ''); ?>

	<script type="text/javascript">
		function postAJAX(urlpath, data, callback, callback_err) {
			$.ajax({
				url: urlpath,
				method: 'POST',
				dataType: 'json',
				data: data,
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
					'Access-Control-Allow-Headers': 'Authorization, Origin, X-Requested-With, Content-Type, Accept',
				},
				success: function(result) {
					console.log('AJAX Result : ', result);
					if (callback) {
						callback(result);
					}
				},
				error: function(err) {
					console.log('AJAX Error : ', err);
					if (callback_err) {
						callback_err(err);
					}
				},
			});
		}

		$(document).ready(function() {

			/* FormSubmit Ajax */
			$(document).on('click', '[form_submit]', function() {
				var action_path = $(this).closest('form').attr('action');
				var formData = $(this).closest('form').serialize();
				postAJAX(action_path, formData, function(data) {
					console.log(data);
				});
			});
			/* FormSubmit Ajax */

		});
	</script>

	<?php
	if (isset($Script) && count($Script) > 0) {
		foreach ($Script as $result) :
			$this->load->view((isset($Module) ? $Module . ($Module != '' ? '/' : '') : '/') . $result);
		endforeach;
	}
	?>

</body>

</html>