<!DOCTYPE html>
<html>

<head>
	<title><?= (isset($Site_Title) ? $Site_Title : (defined(SITE_TITLE) ? SITE_TITLE : "DashQueue")); ?></title>
	<link rel="shortcut icon" href='<?= base_url() ?>static/img/q-logo-Recovered.png'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="content-type" content="text/javascript; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<?php
	$template_css = array(
		'bootstrap-4.4.1' => 'vendor/bootstrap-4.4.1/css/bootstrap.min.css',
		'fontawesome' => 'vendor/fontawesome/css/all.min.css',
		'jqueryDataTable' => 'vendor/jquerydataTable/datatables.min.css',
		'sweetalert' => 'vendor/sweetalert/sweetalert.css',
		'responsivescale' => 'css/ResponsiveScale.css',
		'font' => 'font/pr9/pr9.css'
	);

	$template_js = array(
		'popper' => 'vendor/popper/popper.min.js',
		'jquery3.4.1' => 'js/jquery-3.4.1.min.js',
		'bootstrap-4.4.1' => 'vendor/bootstrap-4.4.1/js/bootstrap.min.js',
		'jqueryDataTable' => 'vendor/jquerydataTable/datatables.min.js',
		'jquery-resizer' => 'vendor/jquery.resize-master/jquery.resize.js',
		'moment' => 'vendor/moment/moment-with-locales.js',
		'sweetalert' => 'vendor/sweetalert/sweetalert.min.js',
		'socketio'=>'vendor/socketio/socket.io.js'

	);

	echo assets_css($template_css);
	?>

	<?= (isset($css) ? assets_css($css) : ''); ?>

	<?php
	if (isset($viewCSS) && count($viewCSS) > 0) {
		foreach ($viewCSS as $result) :
			$this->load->view((isset($Module) ? $Module . ($Module != '' ? '/' : '') : '/') . $result);
		endforeach;
	}
	?>

	<style>
		body {
			font-family: 'dbfongnam_reg';
		}
		.main_h{
			font-family: 'dbozone_med';
		}
		th,h1,h2,h3,h4,h5,h6{
			font-family: 'dbozone_med';
		}
		label{
			font-family: 'dbozone_reg';
		}
		input::placeholder{
			font-family: 'dbozone_light';
		}
		.page_row button{
			font-family: 'dbozone_reg';
		}
	</style>
	<style>
		html,
		body {
			width: 100%;
			height: 100%;
		}

		p {
			margin-bottom: 0rem;
		}

		.bg_color {
			background-color: #008387;
		}

		.center {
			display: flex;
			align-items: center;
			justify-content: center;
			text-align: center;
		}

		.inp {
			height: 15%;
			width: 40%;
			border: 2px solid #e6e6e6;
			border-radius: 26px;
			text-align: center;
			position: absolute;
			margin-top: 4.321%;
			font-size: 2.5rem;
		}

		.bg_img {
			height: 100%;
		}

		.head {
			height: 60px;
			color: #fff;
		}

		.footer {
			height: 60px;
			color: #fff;
		}

		.border_lr {
			border-right: 3px solid #a3bacc;
			border-left: 3px solid #a3bacc;
			padding: 0;
		}

		.modal-header {
			background-color: #008387;
			height: 60px;
			border-radius: .6rem .6rem 0rem 0rem;
		}

		.modal-body {
			border-right: 2px solid #a3bacc;
			border-left: 2px solid #a3bacc;
		}

		.modal-footer {
			padding: 40px;
			padding-top: 0;
			border-top: 0px;
			border-bottom: 2px solid #a3bacc;
			border-right: 2px solid #a3bacc;
			border-left: 2px solid #a3bacc;
			border-radius: 0rem 0rem .6rem .6rem;
		}

		.modal-body>p {
			font-size: 2.8rem;
			color: #008387;
		}

		.modal_box {
			width: 820px;
			display: inline-flex;
			max-width: 820px;
		}

		.btn_pri {
			padding: 10px 0;
		}

		.modal-content {
			border: 0px;
			border-radius: .6rem;
		}
	</style>

</head>

<body>

	<?php
	if (isset($Content) && count($Content) > 0) {
		foreach ($Content as $result) :
			$this->load->view((isset($Module) ? $Module . ($Module != '' ? '/' : '') : '/') . $result);
		endforeach;
	}
	?>

	<?= assets_js($template_js); ?>
	<?= (isset($node_modules) ? assets_node($node_modules) : ''); ?>

	<?php
	if (isset($Script) && count($Script) > 0) {
		foreach ($Script as $result) :
			$this->load->view((isset($Module) ? $Module . ($Module != '' ? '/' : '') : '/') . $result);
		endforeach;
	}
	?>

</body>

</html>